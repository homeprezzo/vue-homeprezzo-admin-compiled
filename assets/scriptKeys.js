export default { 
  json: [
  "localityId",
  "localityIdByIndex",
  "localityType",
  "localityTypeByIndex",
  "shortName",
  "shortNameByIndex",
  "suburbDisplay",
  "suburbDisplayByIndex",
  "suburb",
  "suburbByIndex",
  "suggestion",
  "suggestionByIndex",
  "suggestionType",
  "suggestionTypeByIndex",
  "statsStatsCurrentListingsHouse",
  "statsStatsCurrentListingsHouseByIndex",
  "statsStatsCurrentListingsUnit",
  "statsStatsCurrentListingsUnitByIndex",
  "statsStatsCurrentListingsLand",
  "statsStatsCurrentListingsLandByIndex",
  "statsStatsCurrentListingsTotal",
  "statsStatsCurrentListingsTotalByIndex",
  "statsStatsUnitType",
  "statsStatsUnitTypeByIndex",
  "statsStatsMedianWeeklyHouseholdIncome",
  "statsStatsMedianWeeklyHouseholdIncomeByIndex",
  "statsStatsHouseSalesLast3Months",
  "statsStatsHouseSalesLast3MonthsByIndex",
  "statsStatsHouseSalePriceRangeCurrentHigh",
  "statsStatsHouseSalePriceRangeCurrentHighByIndex",
  "statsStatsHouseSalesLast3MonthsPeriod",
  "statsStatsHouseSalesLast3MonthsPeriodByIndex",
  "statsStatsUnitSalesLast3Months",
  "statsStatsUnitSalesLast3MonthsByIndex",
  "statsStatsUnitSalePriceRangeCurrentHigh",
  "statsStatsUnitSalePriceRangeCurrentHighByIndex",
  "statsStatsUnitSalesLast3MonthsPeriod",
  "statsStatsUnitSalesLast3MonthsPeriodByIndex",
  "statsStatsHouseSalesLast12Months",
  "statsStatsHouseSalesLast12MonthsByIndex",
  "statsStatsHouseSalesLast12MonthsPeriod",
  "statsStatsHouseSalesLast12MonthsPeriodByIndex",
  "statsStatsHouseMedianSalePriceChange1yr",
  "statsStatsHouseMedianSalePriceChange1yrByIndex",
  "statsStatsHouseMedianSalePriceChange3yr",
  "statsStatsHouseMedianSalePriceChange3yrByIndex",
  "statsStatsHouseMedianSalePriceChange5yr",
  "statsStatsHouseMedianSalePriceChange5yrByIndex",
  "statsStatsHouseMedianSalePriceChange10yr",
  "statsStatsHouseMedianSalePriceChange10yrByIndex",
  "statsStatsHouseMedianSalePriceCurrent",
  "statsStatsHouseMedianSalePriceCurrentByIndex",
  "statsStatsHouseMedianSalePriceCurrent12",
  "statsStatsHouseMedianSalePriceCurrent12ByIndex",
  "statsStatsHouseMedianValueLastMonth",
  "statsStatsHouseMedianValueLastMonthByIndex",
  "statsStatsHouseMedianValueQtrChange",
  "statsStatsHouseMedianValueQtrChangeByIndex",
  "statsStatsHouseMedianValue12mthChange",
  "statsStatsHouseMedianValue12mthChangeByIndex",
  "statsStatsHouseMedianValue3yearChange",
  "statsStatsHouseMedianValue3yearChangeByIndex",
  "statsStatsHouseMedianValue5yearChange",
  "statsStatsHouseMedianValue5yearChangeByIndex",
  "statsStatsHouseMedianSalePriceBottom25",
  "statsStatsHouseMedianSalePriceBottom25ByIndex",
  "statsStatsHouseMedianSalePriceTop25",
  "statsStatsHouseMedianSalePriceTop25ByIndex",
  "statsStatsUnitSalesLast12Months",
  "statsStatsUnitSalesLast12MonthsByIndex",
  "statsStatsUnitSalesLast12MonthsPeriod",
  "statsStatsUnitSalesLast12MonthsPeriodByIndex",
  "statsStatsUnitMedianSalePriceChange1yr",
  "statsStatsUnitMedianSalePriceChange1yrByIndex",
  "statsStatsUnitMedianSalePriceChange3yr",
  "statsStatsUnitMedianSalePriceChange3yrByIndex",
  "statsStatsUnitMedianSalePriceChange5yr",
  "statsStatsUnitMedianSalePriceChange5yrByIndex",
  "statsStatsUnitMedianSalePriceChange10yr",
  "statsStatsUnitMedianSalePriceChange10yrByIndex",
  "statsStatsUnitMedianSalePriceCurrent",
  "statsStatsUnitMedianSalePriceCurrentByIndex",
  "statsStatsUnitMedianSalePriceCurrent12",
  "statsStatsUnitMedianSalePriceCurrent12ByIndex",
  "statsStatsUnitMedianValueLastMonth",
  "statsStatsUnitMedianValueLastMonthByIndex",
  "statsStatsUnitMedianValueQtrChange",
  "statsStatsUnitMedianValueQtrChangeByIndex",
  "statsStatsUnitMedianValue12mthChange",
  "statsStatsUnitMedianValue12mthChangeByIndex",
  "statsStatsUnitMedianValue3yearChange",
  "statsStatsUnitMedianValue3yearChangeByIndex",
  "statsStatsUnitMedianValue5yearChange",
  "statsStatsUnitMedianValue5yearChangeByIndex",
  "statsStatsUnitMedianSalePriceBottom25",
  "statsStatsUnitMedianSalePriceBottom25ByIndex",
  "statsStatsUnitMedianSalePriceTop25",
  "statsStatsUnitMedianSalePriceTop25ByIndex",
  "statsStatsLandSalesLast12Months",
  "statsStatsLandSalesLast12MonthsByIndex",
  "statsStatsTotalSalesLast12Months",
  "statsStatsTotalSalesLast12MonthsByIndex",
  "statsStatsLandSalesLast3Months",
  "statsStatsLandSalesLast3MonthsByIndex",
  "statsStatsTotalSalesLast3Months",
  "statsStatsTotalSalesLast3MonthsByIndex",
  "statsStatsAverageTimeOnMarketCurrent",
  "statsStatsAverageTimeOnMarketCurrentByIndex",
  "statsStatsAverageTimeOnMarketCurrentHouse",
  "statsStatsAverageTimeOnMarketCurrentHouseByIndex",
  "statsStatsAverageTimeOnMarketCurrentUnit",
  "statsStatsAverageTimeOnMarketCurrentUnitByIndex",
  "statsStatsAverageTimeOnMarket6mthHouse",
  "statsStatsAverageTimeOnMarket6mthHouseByIndex",
  "statsStatsAverageTimeOnMarket6mthUnit",
  "statsStatsAverageTimeOnMarket6mthUnitByIndex",
  "statsStatsAverageHoldPeriodYears",
  "statsStatsAverageHoldPeriodYearsByIndex",
  "statsStatsAverageHoldPeriodYearsHouse",
  "statsStatsAverageHoldPeriodYearsHouseByIndex",
  "statsStatsAverageHoldPeriodYearsUnit",
  "statsStatsAverageHoldPeriodYearsUnitByIndex",
  "statsStatsMedianVendorDiscountHouse",
  "statsStatsMedianVendorDiscountHouseByIndex",
  "statsStatsMedianVendorDiscountUnit",
  "statsStatsMedianVendorDiscountUnitByIndex",
  "statsStatsIndicativeGrossRentalYieldHouse",
  "statsStatsIndicativeGrossRentalYieldHouseByIndex",
  "statsStatsIndicativeGrossRentalYieldUnit",
  "statsStatsIndicativeGrossRentalYieldUnitByIndex",
  "statsStatsMedianAskingRent12mthHouse",
  "statsStatsMedianAskingRent12mthHouseByIndex",
  "statsStatsMedianAskingRent12mthUnit",
  "statsStatsMedianAskingRent12mthUnitByIndex",
  "statsStatsMedianAskingRent12mthObsHouse",
  "statsStatsMedianAskingRent12mthObsHouseByIndex",
  "statsStatsMedianAskingRent12mthObsUnit",
  "statsStatsMedianAskingRent12mthObsUnitByIndex",
  "statsStatsMedianAskingRent12mthChangeHouse",
  "statsStatsMedianAskingRent12mthChangeHouseByIndex",
  "statsStatsMedianAskingRent12mthChangeUnit",
  "statsStatsMedianAskingRent12mthChangeUnitByIndex",
  "statsStatsMarketListings12Months",
  "statsStatsMarketListings12MonthsByIndex",
  "statsStatsMarketListings12MonthsHouse",
  "statsStatsMarketListings12MonthsHouseByIndex",
  "statsStatsMarketListings12MonthsUnit",
  "statsStatsMarketListings12MonthsUnitByIndex",
  "statsStatsMarketListingsNewLastMonth",
  "statsStatsMarketListingsNewLastMonthByIndex",
  "statsStatsMarketListingsNewLastMonthHouse",
  "statsStatsMarketListingsNewLastMonthHouseByIndex",
  "statsStatsMarketListingsNewLastMonthUnit",
  "statsStatsMarketListingsNewLastMonthUnitByIndex",
  "statsStatsMarketListingsPercentAuction",
  "statsStatsMarketListingsPercentAuctionByIndex",
  "statsStatsPercentofMarketListed12MthHouse",
  "statsStatsPercentofMarketListed12MthHouseByIndex",
  "statsStatsPercentofMarketListed12MthUnit",
  "statsStatsPercentofMarketListed12MthUnitByIndex",
  "statsStatsTotalDwellings",
  "statsStatsTotalDwellingsByIndex",
  "statsStatsHouseDwellings",
  "statsStatsHouseDwellingsByIndex",
  "statsStatsUnitDwellings",
  "statsStatsUnitDwellingsByIndex",
  "salespropertyId",
  "salespropertyIdByIndex",
  "salescontractDate",
  "salescontractDateByIndex",
  "salesisAgentsAdvice",
  "salesisAgentsAdviceByIndex",
  "salesisReaRecentSale",
  "salesisReaRecentSaleByIndex",
  "salesprice",
  "salespriceByIndex",
  "salespriceFormatted",
  "salespriceFormattedByIndex",
  "salessettlementDate",
  "salessettlementDateByIndex",
  "salestype",
  "salestypeByIndex",
  "salesagency",
  "salesagencyByIndex",
  "salesland",
  "saleslandByIndex",
  "salesaddress",
  "salesaddressByIndex",
  "salesshortAddress",
  "salesshortAddressByIndex",
  "salessuburb",
  "salessuburbByIndex",
  "salesbedBathCarbed",
  "salesbedBathCarbedByIndex",
  "salesbedBathCarbath",
  "salesbedBathCarbathByIndex",
  "salesbedBathCarcar",
  "salesbedBathCarcarByIndex",
  "salesgeolat",
  "salesgeolatByIndex",
  "salesgeolong",
  "salesgeolongByIndex",
  "salesphotosthumb",
  "salesphotosthumbByIndex",
  "salesphotoslarge",
  "salesphotoslargeByIndex",
  "salesphoto",
  "salesphotoByIndex",
  "salespropertyType",
  "salespropertyTypeByIndex",
  "salesColsaddress",
  "salesColsaddressByIndex",
  "salesColsagency",
  "salesColsagencyByIndex",
  "salesColsbed",
  "salesColsbedByIndex",
  "salesColsbath",
  "salesColsbathByIndex",
  "salesColscar",
  "salesColscarByIndex",
  "salesColslat",
  "salesColslatByIndex",
  "salesColslng",
  "salesColslngByIndex",
  "salesColscontractDate",
  "salesColscontractDateByIndex",
  "salesColssettlementDate",
  "salesColssettlementDateByIndex",
  "salesColsphoto",
  "salesColsphotoByIndex",
  "salesColsprice",
  "salesColspriceByIndex",
  "salesColspropertyId",
  "salesColspropertyIdByIndex",
  "salesColspropertyType",
  "salesColspropertyTypeByIndex",
  "salesColsshortAddress",
  "salesColsshortAddressByIndex",
  "salesColssuburb",
  "salesColssuburbByIndex",
  "salesColsland",
  "salesColslandByIndex",
  "autoSales",
  "autoSalesByIndex",
  "salesSelectionspropertyId",
  "salesSelectionspropertyIdByIndex",
  "salesSelectionscontractDate",
  "salesSelectionscontractDateByIndex",
  "salesSelectionsisAgentsAdvice",
  "salesSelectionsisAgentsAdviceByIndex",
  "salesSelectionsisReaRecentSale",
  "salesSelectionsisReaRecentSaleByIndex",
  "salesSelectionsprice",
  "salesSelectionspriceByIndex",
  "salesSelectionspriceFormatted",
  "salesSelectionspriceFormattedByIndex",
  "salesSelectionssettlementDate",
  "salesSelectionssettlementDateByIndex",
  "salesSelectionstype",
  "salesSelectionstypeByIndex",
  "salesSelectionsagency",
  "salesSelectionsagencyByIndex",
  "salesSelectionsland",
  "salesSelectionslandByIndex",
  "salesSelectionsaddress",
  "salesSelectionsaddressByIndex",
  "salesSelectionsshortAddress",
  "salesSelectionsshortAddressByIndex",
  "salesSelectionssuburb",
  "salesSelectionssuburbByIndex",
  "salesSelectionsbedBathCarbed",
  "salesSelectionsbedBathCarbedByIndex",
  "salesSelectionsbedBathCarbath",
  "salesSelectionsbedBathCarbathByIndex",
  "salesSelectionsbedBathCarcar",
  "salesSelectionsbedBathCarcarByIndex",
  "salesSelectionsgeolat",
  "salesSelectionsgeolatByIndex",
  "salesSelectionsgeolong",
  "salesSelectionsgeolongByIndex",
  "salesSelectionsphotosthumb",
  "salesSelectionsphotosthumbByIndex",
  "salesSelectionsphotoslarge",
  "salesSelectionsphotoslargeByIndex",
  "salesSelectionsphoto",
  "salesSelectionsphotoByIndex",
  "salesSelectionspropertyType",
  "salesSelectionspropertyTypeByIndex",
  "colorpalette",
  "colorpaletteByIndex",
  "font",
  "fontByIndex",
  "backgroundmusic",
  "backgroundmusicByIndex",
  "voiceover",
  "voiceoverByIndex",
  "frequency",
  "frequencyByIndex",
  "palette",
  "paletteByIndex",
  "userAttrsAgency",
  "userAttrsAgencyByIndex",
  "userAttrsMobile",
  "userAttrsMobileByIndex",
  "userAttrsemail",
  "userAttrsemailByIndex",
  "userAttrsName",
  "userAttrsNameByIndex",
  "userAttrsAgencyLogo",
  "userAttrsAgencyLogoByIndex",
  "userAttrsAgentPhoto",
  "userAttrsAgentPhotoByIndex",
  "userAttrsAddress",
  "userAttrsAddressByIndex",
  "userAttrsWebsite",
  "userAttrsWebsiteByIndex",
  "userAttrsPhoneFax",
  "userAttrsPhoneFaxByIndex",
  "selectionsimage",
  "selectionsimageByIndex",
  "selectionscaption",
  "selectionscaptionByIndex",
  "usedSourcestype",
  "usedSourcestypeByIndex",
  "usedSourceslistthumbnail",
  "usedSourceslistthumbnailByIndex",
  "usedSourceslistcaption",
  "usedSourceslistcaptionByIndex",
  "usedSourcesopt",
  "usedSourcesoptByIndex",
  "userdSourceParams",
  "userdSourceParamsByIndex",
  "bkumpimage",
  "bkumpimageByIndex",
  "bkumpcaption",
  "bkumpcaptionByIndex",
  "bkryimage",
  "bkryimageByIndex",
  "bkrycaption",
  "bkrycaptionByIndex",
  "bkintroimage",
  "bkintroimageByIndex",
  "bkintrocaption",
  "bkintrocaptionByIndex",
  "bktextbox1",
  "bktextbox1ByIndex",
  "bksalesimage",
  "bksalesimageByIndex",
  "bksalescaption",
  "bksalescaptionByIndex",
  "usedSourcesurl",
  "usedSourcesurlByIndex",
  "usedSourcesparametersidtype",
  "usedSourcesparametersidtypeByIndex",
  "usedSourcesparametersidopt",
  "usedSourcesparametersidoptByIndex",
  "usedSourcesparametersidid",
  "usedSourcesparametersididByIndex",
  "usedSourcesparametersidgroupUuid",
  "usedSourcesparametersidgroupUuidByIndex",
  "usedSourcesparametersidsceneUuid",
  "usedSourcesparametersidsceneUuidByIndex",
  "salesPhoto0image",
  "salesPhoto0imageByIndex",
  "salesPhoto0caption",
  "salesPhoto0captionByIndex",
  "bkrsimage",
  "bkrsimageByIndex",
  "bkrscaption",
  "bkrscaptionByIndex",
  "prezzoPropertypropertyDetailaddress",
  "shortenKeys",
  {
    "key": "AgentName",
    "description": "lib.agentName",
    "type": "user",
    "method": "GetUser"
  },
  {
    "key": "AgentMobile",
    "description": "lib.agentMobile",
    "type": "self",
    "method": "GetUser"
  },
  {
    "key": "AgentEmail",
    "description": "lib.agentEmail",
    "type": "self",
    "method": "GetUser"
  },
  {
    "key": "Agency",
    "description": "lib.Agency",
    "type": "self",
    "method": "GetUser"
  },
  {
    "key": "text",
    "description": "",
    "type": "self",
    "method": "MatchSelf"
  },
  {
    "key": "ReviewText",
    "description": "json[lib.group_uuid].review",
    "type": "self",
    "method": "GetReview"
  },
  {
    "key": "ReviewTitle",
    "description": "json[lib.group_uuid].title",
    "type": "self",
    "method": "GetReview"
  },
  {
    "key": "PropBBCcar",
    "target": "sales", 
    "option": ["sales", "listing"],
    "description": "json[lib.group_uuid].prezzoPropertysalesbedBathCarcar[$1]",
    "listingDescription": "json[lib.group_uuid].prezzoPropertylistingsbedBathCarcar[$1]",
    "type": "self",
    "method": "GetPropBBCcar"
  },
  {
    "key": "PropBBCbath",
    "target": "sales",
    "option": ["sales", "listing"],
    "description": "json[lib.group_uuid].prezzoPropertysalesbedBathCarbath[$1]",
    "listingDescription": "json[lib.group_uuid].prezzoPropertylistingsbedBathCarbath[$1]",
    "type": "self",
    "method": "GetPropBBCbath"
  },
  {
    "key": "PropBBCbed",
    "target": "sales",
    "option": ["sales", "listing"],
    "description": "json[lib.group_uuid].prezzoPropertysalesbedBathCarbed[$1]",
    "listingDescription": "json[lib.group_uuid].prezzoPropertylistingsbedBathCarbed[$1]",
    "type": "self",
    "method": "GetPropBBCbed"
  },
  {
    "key": "PrezzoPropSalesAdd",
    "target": "sales",
    "option": ["sales", "listing"],
    "description": "json[lib.group_uuid].prezzoPropertysalesaddress[$1]",
    "listingDescription": "json[lib.group_uuid].prezzoPropertylistingsaddress[$1]",
    "type": "self",
    "method": "GetPrezzoSalesAddress"
  },
  {
    "key": "PropSalePrice",
    "target": "sales",
    "option": ["sales", "listing"],
    "description": "json[lib.group_uuid].prezzoPropertysalespriceFormatted[$1]",
    "listingDescription": "json[lib.group_uuid].prezzoPropertylistingpriceFormatted[$1]",
    "type": "self",
    "method": "GetPrezzoSalePrice"
  },
  
  ]
}
