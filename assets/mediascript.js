// require('./cjs.js')
const cjs = createjs;
export default {
    grabfunction (script, canvas) {
        console.log(cjs)
        (function (cjs, an) {
            console.log(cjs)
        var p; // shortcut to reference prototypes
        var lib={};var ss={};var img={};
        lib.ssMetadata = [];

        (lib.MBBC = function(mode,startPosition,loop) {
            this.initialize(mode,startPosition,loop,{});

            // Layer_1
            this.shape = new cjs.Shape();
            this.shape.graphics.f("#333333").s().p("Ehj/A4QMAAAhwfMDH/AAAMAAABwfg");
            this.shape.setTransform(640,360);

            this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

        }).prototype = p = new cjs.MovieClip();
        p.nominalBounds = new cjs.Rectangle(0,0,1280,720);


        (lib.Frames_96 = function(mode,startPosition,loop) {
            this.initialize(mode,startPosition,loop,{});

            // Layer_1
            this.shape = new cjs.Shape();
            this.shape.graphics.f("#333333").s().p("Ehj/A4QMAAAhwfMDH/AAAMAAABwfg");
            this.shape.setTransform(640,360);

            this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

        }).prototype = p = new cjs.MovieClip();
        p.nominalBounds = new cjs.Rectangle(0,0,1280,720);


        (lib.Tween1 = function(mode,startPosition,loop) {
            this.initialize(mode,startPosition,loop,{});

            // Layer_1
            this.shape = new cjs.Shape();
            this.shape.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape.setTransform(617.8973,338.3716,0.7277,0.3104);

            this.shape_1 = new cjs.Shape();
            this.shape_1.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_1.setTransform(617.7716,339.1528,0.7277,0.3104,-90);

            this.shape_2 = new cjs.Shape();
            this.shape_2.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_2.setTransform(617.8973,276.8716,0.7277,0.3104);

            this.shape_3 = new cjs.Shape();
            this.shape_3.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_3.setTransform(617.7716,277.4028,0.7277,0.3104,-90);

            this.shape_4 = new cjs.Shape();
            this.shape_4.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_4.setTransform(617.8973,217.4216,0.7277,0.3104);

            this.shape_5 = new cjs.Shape();
            this.shape_5.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_5.setTransform(617.7716,218.1028,0.7277,0.3104,-90);

            this.shape_6 = new cjs.Shape();
            this.shape_6.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_6.setTransform(617.8973,155.5716,0.7277,0.3104);

            this.shape_7 = new cjs.Shape();
            this.shape_7.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_7.setTransform(617.7716,156.0028,0.7277,0.3104,-90);

            this.shape_8 = new cjs.Shape();
            this.shape_8.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_8.setTransform(617.8973,87.5716,0.7277,0.3104);

            this.shape_9 = new cjs.Shape();
            this.shape_9.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_9.setTransform(617.7716,88.0028,0.7277,0.3104,-90);

            this.shape_10 = new cjs.Shape();
            this.shape_10.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_10.setTransform(617.8973,25.9716,0.7277,0.3104);

            this.shape_11 = new cjs.Shape();
            this.shape_11.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_11.setTransform(617.7716,26.1028,0.7277,0.3104,-90);

            this.shape_12 = new cjs.Shape();
            this.shape_12.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_12.setTransform(617.8973,-34.2284,0.7277,0.3104);

            this.shape_13 = new cjs.Shape();
            this.shape_13.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_13.setTransform(617.7716,-33.0972,0.7277,0.3104,-90);

            this.shape_14 = new cjs.Shape();
            this.shape_14.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_14.setTransform(617.8973,-95.1784,0.7277,0.3104);

            this.shape_15 = new cjs.Shape();
            this.shape_15.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_15.setTransform(617.7716,-94.6472,0.7277,0.3104,-90);

            this.shape_16 = new cjs.Shape();
            this.shape_16.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_16.setTransform(617.8973,-154.9284,0.7277,0.3104);

            this.shape_17 = new cjs.Shape();
            this.shape_17.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_17.setTransform(617.7716,-154.5972,0.7277,0.3104,-90);

            this.shape_18 = new cjs.Shape();
            this.shape_18.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_18.setTransform(617.8973,-216.7284,0.7277,0.3104);

            this.shape_19 = new cjs.Shape();
            this.shape_19.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_19.setTransform(617.7716,-216.0472,0.7277,0.3104,-90);

            this.shape_20 = new cjs.Shape();
            this.shape_20.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_20.setTransform(617.8973,-277.0284,0.7277,0.3104);

            this.shape_21 = new cjs.Shape();
            this.shape_21.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_21.setTransform(617.7716,-276.3472,0.7277,0.3104,-90);

            this.shape_22 = new cjs.Shape();
            this.shape_22.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_22.setTransform(617.8973,-338.3784,0.7277,0.3104);

            this.shape_23 = new cjs.Shape();
            this.shape_23.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_23.setTransform(617.7716,-338.1972,0.7277,0.3104,-90);

            this.shape_24 = new cjs.Shape();
            this.shape_24.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_24.setTransform(567.2973,338.3716,0.7277,0.3104);

            this.shape_25 = new cjs.Shape();
            this.shape_25.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_25.setTransform(567.8216,339.1528,0.7277,0.3104,-90);

            this.shape_26 = new cjs.Shape();
            this.shape_26.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_26.setTransform(567.2973,276.8716,0.7277,0.3104);

            this.shape_27 = new cjs.Shape();
            this.shape_27.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_27.setTransform(567.8216,277.4028,0.7277,0.3104,-90);

            this.shape_28 = new cjs.Shape();
            this.shape_28.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_28.setTransform(567.2973,217.4216,0.7277,0.3104);

            this.shape_29 = new cjs.Shape();
            this.shape_29.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_29.setTransform(567.8216,218.1028,0.7277,0.3104,-90);

            this.shape_30 = new cjs.Shape();
            this.shape_30.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_30.setTransform(567.2973,155.5716,0.7277,0.3104);

            this.shape_31 = new cjs.Shape();
            this.shape_31.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_31.setTransform(567.8216,156.0028,0.7277,0.3104,-90);

            this.shape_32 = new cjs.Shape();
            this.shape_32.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_32.setTransform(567.2973,87.5716,0.7277,0.3104);

            this.shape_33 = new cjs.Shape();
            this.shape_33.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_33.setTransform(567.8216,88.0028,0.7277,0.3104,-90);

            this.shape_34 = new cjs.Shape();
            this.shape_34.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_34.setTransform(567.2973,25.9716,0.7277,0.3104);

            this.shape_35 = new cjs.Shape();
            this.shape_35.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_35.setTransform(567.8216,26.1028,0.7277,0.3104,-90);

            this.shape_36 = new cjs.Shape();
            this.shape_36.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_36.setTransform(567.2973,-34.2284,0.7277,0.3104);

            this.shape_37 = new cjs.Shape();
            this.shape_37.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_37.setTransform(567.8216,-33.0972,0.7277,0.3104,-90);

            this.shape_38 = new cjs.Shape();
            this.shape_38.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_38.setTransform(567.2973,-95.1784,0.7277,0.3104);

            this.shape_39 = new cjs.Shape();
            this.shape_39.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_39.setTransform(567.8216,-94.6472,0.7277,0.3104,-90);

            this.shape_40 = new cjs.Shape();
            this.shape_40.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_40.setTransform(567.2973,-154.9284,0.7277,0.3104);

            this.shape_41 = new cjs.Shape();
            this.shape_41.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_41.setTransform(567.8216,-154.5972,0.7277,0.3104,-90);

            this.shape_42 = new cjs.Shape();
            this.shape_42.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_42.setTransform(567.2973,-216.7284,0.7277,0.3104);

            this.shape_43 = new cjs.Shape();
            this.shape_43.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_43.setTransform(567.8216,-216.0472,0.7277,0.3104,-90);

            this.shape_44 = new cjs.Shape();
            this.shape_44.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_44.setTransform(567.2973,-277.0284,0.7277,0.3104);

            this.shape_45 = new cjs.Shape();
            this.shape_45.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_45.setTransform(567.8216,-276.3472,0.7277,0.3104,-90);

            this.shape_46 = new cjs.Shape();
            this.shape_46.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_46.setTransform(567.2973,-338.3784,0.7277,0.3104);

            this.shape_47 = new cjs.Shape();
            this.shape_47.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_47.setTransform(567.8216,-338.1972,0.7277,0.3104,-90);

            this.shape_48 = new cjs.Shape();
            this.shape_48.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_48.setTransform(522.2473,338.3716,0.7277,0.3104);

            this.shape_49 = new cjs.Shape();
            this.shape_49.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_49.setTransform(522.3216,339.1528,0.7277,0.3104,-90);

            this.shape_50 = new cjs.Shape();
            this.shape_50.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_50.setTransform(522.2473,276.8716,0.7277,0.3104);

            this.shape_51 = new cjs.Shape();
            this.shape_51.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_51.setTransform(522.3216,277.4028,0.7277,0.3104,-90);

            this.shape_52 = new cjs.Shape();
            this.shape_52.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_52.setTransform(522.2473,217.4216,0.7277,0.3104);

            this.shape_53 = new cjs.Shape();
            this.shape_53.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_53.setTransform(522.3216,218.1028,0.7277,0.3104,-90);

            this.shape_54 = new cjs.Shape();
            this.shape_54.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_54.setTransform(522.2473,155.5716,0.7277,0.3104);

            this.shape_55 = new cjs.Shape();
            this.shape_55.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_55.setTransform(522.3216,156.0028,0.7277,0.3104,-90);

            this.shape_56 = new cjs.Shape();
            this.shape_56.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_56.setTransform(522.2473,87.5716,0.7277,0.3104);

            this.shape_57 = new cjs.Shape();
            this.shape_57.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_57.setTransform(522.3216,88.0028,0.7277,0.3104,-90);

            this.shape_58 = new cjs.Shape();
            this.shape_58.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_58.setTransform(522.2473,25.9716,0.7277,0.3104);

            this.shape_59 = new cjs.Shape();
            this.shape_59.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_59.setTransform(522.3216,26.1028,0.7277,0.3104,-90);

            this.shape_60 = new cjs.Shape();
            this.shape_60.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_60.setTransform(522.2473,-34.2284,0.7277,0.3104);

            this.shape_61 = new cjs.Shape();
            this.shape_61.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_61.setTransform(522.3216,-33.0972,0.7277,0.3104,-90);

            this.shape_62 = new cjs.Shape();
            this.shape_62.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_62.setTransform(522.2473,-95.1784,0.7277,0.3104);

            this.shape_63 = new cjs.Shape();
            this.shape_63.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_63.setTransform(522.3216,-94.6472,0.7277,0.3104,-90);

            this.shape_64 = new cjs.Shape();
            this.shape_64.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_64.setTransform(522.2473,-154.9284,0.7277,0.3104);

            this.shape_65 = new cjs.Shape();
            this.shape_65.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_65.setTransform(522.3216,-154.5972,0.7277,0.3104,-90);

            this.shape_66 = new cjs.Shape();
            this.shape_66.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_66.setTransform(522.2473,-216.7284,0.7277,0.3104);

            this.shape_67 = new cjs.Shape();
            this.shape_67.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_67.setTransform(522.3216,-216.0472,0.7277,0.3104,-90);

            this.shape_68 = new cjs.Shape();
            this.shape_68.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_68.setTransform(522.2473,-277.0284,0.7277,0.3104);

            this.shape_69 = new cjs.Shape();
            this.shape_69.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_69.setTransform(522.3216,-276.3472,0.7277,0.3104,-90);

            this.shape_70 = new cjs.Shape();
            this.shape_70.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_70.setTransform(522.2473,-338.3784,0.7277,0.3104);

            this.shape_71 = new cjs.Shape();
            this.shape_71.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_71.setTransform(522.3216,-338.1972,0.7277,0.3104,-90);

            this.shape_72 = new cjs.Shape();
            this.shape_72.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_72.setTransform(475.4473,338.3716,0.7277,0.3104);

            this.shape_73 = new cjs.Shape();
            this.shape_73.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_73.setTransform(475.1716,339.1528,0.7277,0.3104,-90);

            this.shape_74 = new cjs.Shape();
            this.shape_74.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_74.setTransform(475.4473,276.8716,0.7277,0.3104);

            this.shape_75 = new cjs.Shape();
            this.shape_75.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_75.setTransform(475.1716,277.4028,0.7277,0.3104,-90);

            this.shape_76 = new cjs.Shape();
            this.shape_76.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_76.setTransform(475.4473,217.4216,0.7277,0.3104);

            this.shape_77 = new cjs.Shape();
            this.shape_77.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_77.setTransform(475.1716,218.1028,0.7277,0.3104,-90);

            this.shape_78 = new cjs.Shape();
            this.shape_78.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_78.setTransform(475.4473,155.5716,0.7277,0.3104);

            this.shape_79 = new cjs.Shape();
            this.shape_79.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_79.setTransform(475.1716,156.0028,0.7277,0.3104,-90);

            this.shape_80 = new cjs.Shape();
            this.shape_80.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_80.setTransform(475.4473,87.5716,0.7277,0.3104);

            this.shape_81 = new cjs.Shape();
            this.shape_81.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_81.setTransform(475.1716,88.0028,0.7277,0.3104,-90);

            this.shape_82 = new cjs.Shape();
            this.shape_82.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_82.setTransform(475.4473,25.9716,0.7277,0.3104);

            this.shape_83 = new cjs.Shape();
            this.shape_83.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_83.setTransform(475.1716,26.1028,0.7277,0.3104,-90);

            this.shape_84 = new cjs.Shape();
            this.shape_84.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_84.setTransform(475.4473,-34.2284,0.7277,0.3104);

            this.shape_85 = new cjs.Shape();
            this.shape_85.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_85.setTransform(475.1716,-33.0972,0.7277,0.3104,-90);

            this.shape_86 = new cjs.Shape();
            this.shape_86.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_86.setTransform(475.4473,-95.1784,0.7277,0.3104);

            this.shape_87 = new cjs.Shape();
            this.shape_87.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_87.setTransform(475.1716,-94.6472,0.7277,0.3104,-90);

            this.shape_88 = new cjs.Shape();
            this.shape_88.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_88.setTransform(475.4473,-154.9284,0.7277,0.3104);

            this.shape_89 = new cjs.Shape();
            this.shape_89.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_89.setTransform(475.1716,-154.5972,0.7277,0.3104,-90);

            this.shape_90 = new cjs.Shape();
            this.shape_90.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_90.setTransform(475.4473,-216.7284,0.7277,0.3104);

            this.shape_91 = new cjs.Shape();
            this.shape_91.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_91.setTransform(475.1716,-216.0472,0.7277,0.3104,-90);

            this.shape_92 = new cjs.Shape();
            this.shape_92.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_92.setTransform(475.4473,-277.0284,0.7277,0.3104);

            this.shape_93 = new cjs.Shape();
            this.shape_93.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_93.setTransform(475.1716,-276.3472,0.7277,0.3104,-90);

            this.shape_94 = new cjs.Shape();
            this.shape_94.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_94.setTransform(475.4473,-338.3784,0.7277,0.3104);

            this.shape_95 = new cjs.Shape();
            this.shape_95.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_95.setTransform(475.1716,-338.1972,0.7277,0.3104,-90);

            this.shape_96 = new cjs.Shape();
            this.shape_96.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_96.setTransform(430.1973,338.3716,0.7277,0.3104);

            this.shape_97 = new cjs.Shape();
            this.shape_97.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_97.setTransform(430.0216,339.1528,0.7277,0.3104,-90);

            this.shape_98 = new cjs.Shape();
            this.shape_98.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_98.setTransform(430.1973,276.8716,0.7277,0.3104);

            this.shape_99 = new cjs.Shape();
            this.shape_99.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_99.setTransform(430.0216,277.4028,0.7277,0.3104,-90);

            this.shape_100 = new cjs.Shape();
            this.shape_100.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_100.setTransform(430.1973,217.4216,0.7277,0.3104);

            this.shape_101 = new cjs.Shape();
            this.shape_101.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_101.setTransform(430.0216,218.1028,0.7277,0.3104,-90);

            this.shape_102 = new cjs.Shape();
            this.shape_102.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_102.setTransform(430.1973,155.5716,0.7277,0.3104);

            this.shape_103 = new cjs.Shape();
            this.shape_103.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_103.setTransform(430.0216,156.0028,0.7277,0.3104,-90);

            this.shape_104 = new cjs.Shape();
            this.shape_104.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_104.setTransform(430.1973,87.5716,0.7277,0.3104);

            this.shape_105 = new cjs.Shape();
            this.shape_105.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_105.setTransform(430.0216,88.0028,0.7277,0.3104,-90);

            this.shape_106 = new cjs.Shape();
            this.shape_106.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_106.setTransform(430.1973,25.9716,0.7277,0.3104);

            this.shape_107 = new cjs.Shape();
            this.shape_107.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_107.setTransform(430.0216,26.1028,0.7277,0.3104,-90);

            this.shape_108 = new cjs.Shape();
            this.shape_108.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_108.setTransform(430.1973,-34.2284,0.7277,0.3104);

            this.shape_109 = new cjs.Shape();
            this.shape_109.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_109.setTransform(430.0216,-33.0972,0.7277,0.3104,-90);

            this.shape_110 = new cjs.Shape();
            this.shape_110.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_110.setTransform(430.1973,-95.1784,0.7277,0.3104);

            this.shape_111 = new cjs.Shape();
            this.shape_111.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_111.setTransform(430.0216,-94.6472,0.7277,0.3104,-90);

            this.shape_112 = new cjs.Shape();
            this.shape_112.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_112.setTransform(430.1973,-154.9284,0.7277,0.3104);

            this.shape_113 = new cjs.Shape();
            this.shape_113.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_113.setTransform(430.0216,-154.5972,0.7277,0.3104,-90);

            this.shape_114 = new cjs.Shape();
            this.shape_114.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_114.setTransform(430.1973,-216.7284,0.7277,0.3104);

            this.shape_115 = new cjs.Shape();
            this.shape_115.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_115.setTransform(430.0216,-216.0472,0.7277,0.3104,-90);

            this.shape_116 = new cjs.Shape();
            this.shape_116.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_116.setTransform(430.1973,-277.0284,0.7277,0.3104);

            this.shape_117 = new cjs.Shape();
            this.shape_117.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_117.setTransform(430.0216,-276.3472,0.7277,0.3104,-90);

            this.shape_118 = new cjs.Shape();
            this.shape_118.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_118.setTransform(430.1973,-338.3784,0.7277,0.3104);

            this.shape_119 = new cjs.Shape();
            this.shape_119.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_119.setTransform(430.0216,-338.1972,0.7277,0.3104,-90);

            this.shape_120 = new cjs.Shape();
            this.shape_120.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_120.setTransform(379.8472,338.3716,0.7277,0.3104);

            this.shape_121 = new cjs.Shape();
            this.shape_121.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_121.setTransform(379.6716,339.1528,0.7277,0.3104,-90);

            this.shape_122 = new cjs.Shape();
            this.shape_122.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_122.setTransform(379.8472,276.8716,0.7277,0.3104);

            this.shape_123 = new cjs.Shape();
            this.shape_123.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_123.setTransform(379.6716,277.4028,0.7277,0.3104,-90);

            this.shape_124 = new cjs.Shape();
            this.shape_124.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_124.setTransform(379.8472,217.4216,0.7277,0.3104);

            this.shape_125 = new cjs.Shape();
            this.shape_125.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_125.setTransform(379.6716,218.1028,0.7277,0.3104,-90);

            this.shape_126 = new cjs.Shape();
            this.shape_126.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_126.setTransform(379.8472,155.5716,0.7277,0.3104);

            this.shape_127 = new cjs.Shape();
            this.shape_127.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_127.setTransform(379.6716,156.0028,0.7277,0.3104,-90);

            this.shape_128 = new cjs.Shape();
            this.shape_128.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_128.setTransform(379.8472,87.5716,0.7277,0.3104);

            this.shape_129 = new cjs.Shape();
            this.shape_129.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_129.setTransform(379.6716,88.0028,0.7277,0.3104,-90);

            this.shape_130 = new cjs.Shape();
            this.shape_130.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_130.setTransform(379.8472,25.9716,0.7277,0.3104);

            this.shape_131 = new cjs.Shape();
            this.shape_131.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_131.setTransform(379.6716,26.1028,0.7277,0.3104,-90);

            this.shape_132 = new cjs.Shape();
            this.shape_132.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_132.setTransform(379.8472,-34.2284,0.7277,0.3104);

            this.shape_133 = new cjs.Shape();
            this.shape_133.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_133.setTransform(379.6716,-33.0972,0.7277,0.3104,-90);

            this.shape_134 = new cjs.Shape();
            this.shape_134.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_134.setTransform(379.8472,-95.1784,0.7277,0.3104);

            this.shape_135 = new cjs.Shape();
            this.shape_135.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_135.setTransform(379.6716,-94.6472,0.7277,0.3104,-90);

            this.shape_136 = new cjs.Shape();
            this.shape_136.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_136.setTransform(379.8472,-154.9284,0.7277,0.3104);

            this.shape_137 = new cjs.Shape();
            this.shape_137.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_137.setTransform(379.6716,-154.5972,0.7277,0.3104,-90);

            this.shape_138 = new cjs.Shape();
            this.shape_138.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_138.setTransform(379.8472,-216.7284,0.7277,0.3104);

            this.shape_139 = new cjs.Shape();
            this.shape_139.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_139.setTransform(379.6716,-216.0472,0.7277,0.3104,-90);

            this.shape_140 = new cjs.Shape();
            this.shape_140.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_140.setTransform(379.8472,-277.0284,0.7277,0.3104);

            this.shape_141 = new cjs.Shape();
            this.shape_141.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_141.setTransform(379.6716,-276.3472,0.7277,0.3104,-90);

            this.shape_142 = new cjs.Shape();
            this.shape_142.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_142.setTransform(379.8472,-338.3784,0.7277,0.3104);

            this.shape_143 = new cjs.Shape();
            this.shape_143.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_143.setTransform(379.6716,-338.1972,0.7277,0.3104,-90);

            this.shape_144 = new cjs.Shape();
            this.shape_144.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_144.setTransform(334.5473,338.3716,0.7277,0.3104);

            this.shape_145 = new cjs.Shape();
            this.shape_145.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_145.setTransform(334.4716,339.1528,0.7277,0.3104,-90);

            this.shape_146 = new cjs.Shape();
            this.shape_146.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_146.setTransform(334.5473,276.8716,0.7277,0.3104);

            this.shape_147 = new cjs.Shape();
            this.shape_147.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_147.setTransform(334.4716,277.4028,0.7277,0.3104,-90);

            this.shape_148 = new cjs.Shape();
            this.shape_148.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_148.setTransform(334.5473,217.4216,0.7277,0.3104);

            this.shape_149 = new cjs.Shape();
            this.shape_149.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_149.setTransform(334.4716,218.1028,0.7277,0.3104,-90);

            this.shape_150 = new cjs.Shape();
            this.shape_150.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_150.setTransform(334.5473,155.5716,0.7277,0.3104);

            this.shape_151 = new cjs.Shape();
            this.shape_151.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_151.setTransform(334.4716,156.0028,0.7277,0.3104,-90);

            this.shape_152 = new cjs.Shape();
            this.shape_152.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_152.setTransform(334.5473,87.5716,0.7277,0.3104);

            this.shape_153 = new cjs.Shape();
            this.shape_153.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_153.setTransform(334.4716,88.0028,0.7277,0.3104,-90);

            this.shape_154 = new cjs.Shape();
            this.shape_154.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_154.setTransform(334.5473,25.9716,0.7277,0.3104);

            this.shape_155 = new cjs.Shape();
            this.shape_155.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_155.setTransform(334.4716,26.1028,0.7277,0.3104,-90);

            this.shape_156 = new cjs.Shape();
            this.shape_156.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_156.setTransform(334.5473,-34.2284,0.7277,0.3104);

            this.shape_157 = new cjs.Shape();
            this.shape_157.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_157.setTransform(334.4716,-33.0972,0.7277,0.3104,-90);

            this.shape_158 = new cjs.Shape();
            this.shape_158.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_158.setTransform(334.5473,-95.1784,0.7277,0.3104);

            this.shape_159 = new cjs.Shape();
            this.shape_159.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_159.setTransform(334.4716,-94.6472,0.7277,0.3104,-90);

            this.shape_160 = new cjs.Shape();
            this.shape_160.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_160.setTransform(334.5473,-154.9284,0.7277,0.3104);

            this.shape_161 = new cjs.Shape();
            this.shape_161.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_161.setTransform(334.4716,-154.5972,0.7277,0.3104,-90);

            this.shape_162 = new cjs.Shape();
            this.shape_162.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_162.setTransform(334.5473,-216.7284,0.7277,0.3104);

            this.shape_163 = new cjs.Shape();
            this.shape_163.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_163.setTransform(334.4716,-216.0472,0.7277,0.3104,-90);

            this.shape_164 = new cjs.Shape();
            this.shape_164.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_164.setTransform(334.5473,-277.0284,0.7277,0.3104);

            this.shape_165 = new cjs.Shape();
            this.shape_165.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_165.setTransform(334.4716,-276.3472,0.7277,0.3104,-90);

            this.shape_166 = new cjs.Shape();
            this.shape_166.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_166.setTransform(334.5473,-338.3784,0.7277,0.3104);

            this.shape_167 = new cjs.Shape();
            this.shape_167.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_167.setTransform(334.4716,-338.1972,0.7277,0.3104,-90);

            this.shape_168 = new cjs.Shape();
            this.shape_168.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_168.setTransform(285.6973,338.3716,0.7277,0.3104);

            this.shape_169 = new cjs.Shape();
            this.shape_169.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_169.setTransform(286.0216,339.1528,0.7277,0.3104,-90);

            this.shape_170 = new cjs.Shape();
            this.shape_170.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_170.setTransform(285.6973,276.8716,0.7277,0.3104);

            this.shape_171 = new cjs.Shape();
            this.shape_171.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_171.setTransform(286.0216,277.4028,0.7277,0.3104,-90);

            this.shape_172 = new cjs.Shape();
            this.shape_172.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_172.setTransform(285.6973,217.4216,0.7277,0.3104);

            this.shape_173 = new cjs.Shape();
            this.shape_173.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_173.setTransform(286.0216,218.1028,0.7277,0.3104,-90);

            this.shape_174 = new cjs.Shape();
            this.shape_174.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_174.setTransform(285.6973,155.5716,0.7277,0.3104);

            this.shape_175 = new cjs.Shape();
            this.shape_175.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_175.setTransform(286.0216,156.0028,0.7277,0.3104,-90);

            this.shape_176 = new cjs.Shape();
            this.shape_176.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_176.setTransform(285.6973,87.5716,0.7277,0.3104);

            this.shape_177 = new cjs.Shape();
            this.shape_177.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_177.setTransform(286.0216,88.0028,0.7277,0.3104,-90);

            this.shape_178 = new cjs.Shape();
            this.shape_178.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_178.setTransform(285.6973,25.9716,0.7277,0.3104);

            this.shape_179 = new cjs.Shape();
            this.shape_179.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_179.setTransform(286.0216,26.1028,0.7277,0.3104,-90);

            this.shape_180 = new cjs.Shape();
            this.shape_180.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_180.setTransform(285.6973,-34.2284,0.7277,0.3104);

            this.shape_181 = new cjs.Shape();
            this.shape_181.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_181.setTransform(286.0216,-33.0972,0.7277,0.3104,-90);

            this.shape_182 = new cjs.Shape();
            this.shape_182.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_182.setTransform(285.6973,-95.1784,0.7277,0.3104);

            this.shape_183 = new cjs.Shape();
            this.shape_183.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_183.setTransform(286.0216,-94.6472,0.7277,0.3104,-90);

            this.shape_184 = new cjs.Shape();
            this.shape_184.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_184.setTransform(285.6973,-154.9284,0.7277,0.3104);

            this.shape_185 = new cjs.Shape();
            this.shape_185.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_185.setTransform(286.0216,-154.5972,0.7277,0.3104,-90);

            this.shape_186 = new cjs.Shape();
            this.shape_186.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_186.setTransform(285.6973,-216.7284,0.7277,0.3104);

            this.shape_187 = new cjs.Shape();
            this.shape_187.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_187.setTransform(286.0216,-216.0472,0.7277,0.3104,-90);

            this.shape_188 = new cjs.Shape();
            this.shape_188.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_188.setTransform(285.6973,-277.0284,0.7277,0.3104);

            this.shape_189 = new cjs.Shape();
            this.shape_189.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_189.setTransform(286.0216,-276.3472,0.7277,0.3104,-90);

            this.shape_190 = new cjs.Shape();
            this.shape_190.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_190.setTransform(285.6973,-338.3784,0.7277,0.3104);

            this.shape_191 = new cjs.Shape();
            this.shape_191.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_191.setTransform(286.0216,-338.1972,0.7277,0.3104,-90);

            this.shape_192 = new cjs.Shape();
            this.shape_192.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_192.setTransform(240.3473,338.3716,0.7277,0.3104);

            this.shape_193 = new cjs.Shape();
            this.shape_193.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_193.setTransform(240.4716,339.1528,0.7277,0.3104,-90);

            this.shape_194 = new cjs.Shape();
            this.shape_194.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_194.setTransform(240.3473,276.8716,0.7277,0.3104);

            this.shape_195 = new cjs.Shape();
            this.shape_195.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_195.setTransform(240.4716,277.4028,0.7277,0.3104,-90);

            this.shape_196 = new cjs.Shape();
            this.shape_196.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_196.setTransform(240.3473,217.4216,0.7277,0.3104);

            this.shape_197 = new cjs.Shape();
            this.shape_197.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_197.setTransform(240.4716,218.1028,0.7277,0.3104,-90);

            this.shape_198 = new cjs.Shape();
            this.shape_198.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_198.setTransform(240.3473,155.5716,0.7277,0.3104);

            this.shape_199 = new cjs.Shape();
            this.shape_199.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_199.setTransform(240.4716,156.0028,0.7277,0.3104,-90);

            this.shape_200 = new cjs.Shape();
            this.shape_200.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_200.setTransform(240.3473,87.5716,0.7277,0.3104);

            this.shape_201 = new cjs.Shape();
            this.shape_201.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_201.setTransform(240.4716,88.0028,0.7277,0.3104,-90);

            this.shape_202 = new cjs.Shape();
            this.shape_202.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_202.setTransform(240.3473,25.9716,0.7277,0.3104);

            this.shape_203 = new cjs.Shape();
            this.shape_203.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_203.setTransform(240.4716,26.1028,0.7277,0.3104,-90);

            this.shape_204 = new cjs.Shape();
            this.shape_204.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_204.setTransform(240.3473,-34.2284,0.7277,0.3104);

            this.shape_205 = new cjs.Shape();
            this.shape_205.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_205.setTransform(240.4716,-33.0972,0.7277,0.3104,-90);

            this.shape_206 = new cjs.Shape();
            this.shape_206.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_206.setTransform(240.3473,-95.1784,0.7277,0.3104);

            this.shape_207 = new cjs.Shape();
            this.shape_207.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_207.setTransform(240.4716,-94.6472,0.7277,0.3104,-90);

            this.shape_208 = new cjs.Shape();
            this.shape_208.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_208.setTransform(240.3473,-154.9284,0.7277,0.3104);

            this.shape_209 = new cjs.Shape();
            this.shape_209.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_209.setTransform(240.4716,-154.5972,0.7277,0.3104,-90);

            this.shape_210 = new cjs.Shape();
            this.shape_210.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_210.setTransform(240.3473,-216.7284,0.7277,0.3104);

            this.shape_211 = new cjs.Shape();
            this.shape_211.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_211.setTransform(240.4716,-216.0472,0.7277,0.3104,-90);

            this.shape_212 = new cjs.Shape();
            this.shape_212.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_212.setTransform(240.3473,-277.0284,0.7277,0.3104);

            this.shape_213 = new cjs.Shape();
            this.shape_213.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_213.setTransform(240.4716,-276.3472,0.7277,0.3104,-90);

            this.shape_214 = new cjs.Shape();
            this.shape_214.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_214.setTransform(240.3473,-338.3784,0.7277,0.3104);

            this.shape_215 = new cjs.Shape();
            this.shape_215.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_215.setTransform(240.4716,-338.1972,0.7277,0.3104,-90);

            this.shape_216 = new cjs.Shape();
            this.shape_216.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_216.setTransform(190.2473,338.3716,0.7277,0.3104);

            this.shape_217 = new cjs.Shape();
            this.shape_217.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_217.setTransform(190.1216,339.1528,0.7277,0.3104,-90);

            this.shape_218 = new cjs.Shape();
            this.shape_218.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_218.setTransform(190.2473,276.8716,0.7277,0.3104);

            this.shape_219 = new cjs.Shape();
            this.shape_219.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_219.setTransform(190.1216,277.4028,0.7277,0.3104,-90);

            this.shape_220 = new cjs.Shape();
            this.shape_220.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_220.setTransform(190.2473,217.4216,0.7277,0.3104);

            this.shape_221 = new cjs.Shape();
            this.shape_221.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_221.setTransform(190.1216,218.1028,0.7277,0.3104,-90);

            this.shape_222 = new cjs.Shape();
            this.shape_222.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_222.setTransform(190.2473,155.5716,0.7277,0.3104);

            this.shape_223 = new cjs.Shape();
            this.shape_223.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_223.setTransform(190.1216,156.0028,0.7277,0.3104,-90);

            this.shape_224 = new cjs.Shape();
            this.shape_224.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_224.setTransform(190.2473,87.5716,0.7277,0.3104);

            this.shape_225 = new cjs.Shape();
            this.shape_225.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_225.setTransform(190.1216,88.0028,0.7277,0.3104,-90);

            this.shape_226 = new cjs.Shape();
            this.shape_226.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_226.setTransform(190.2473,25.9716,0.7277,0.3104);

            this.shape_227 = new cjs.Shape();
            this.shape_227.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_227.setTransform(190.1216,26.1028,0.7277,0.3104,-90);

            this.shape_228 = new cjs.Shape();
            this.shape_228.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_228.setTransform(190.2473,-34.2284,0.7277,0.3104);

            this.shape_229 = new cjs.Shape();
            this.shape_229.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_229.setTransform(190.1216,-33.0972,0.7277,0.3104,-90);

            this.shape_230 = new cjs.Shape();
            this.shape_230.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_230.setTransform(190.2473,-95.1784,0.7277,0.3104);

            this.shape_231 = new cjs.Shape();
            this.shape_231.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_231.setTransform(190.1216,-94.6472,0.7277,0.3104,-90);

            this.shape_232 = new cjs.Shape();
            this.shape_232.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_232.setTransform(190.2473,-154.9284,0.7277,0.3104);

            this.shape_233 = new cjs.Shape();
            this.shape_233.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_233.setTransform(190.1216,-154.5972,0.7277,0.3104,-90);

            this.shape_234 = new cjs.Shape();
            this.shape_234.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_234.setTransform(190.2473,-216.7284,0.7277,0.3104);

            this.shape_235 = new cjs.Shape();
            this.shape_235.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_235.setTransform(190.1216,-216.0472,0.7277,0.3104,-90);

            this.shape_236 = new cjs.Shape();
            this.shape_236.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_236.setTransform(190.2473,-277.0284,0.7277,0.3104);

            this.shape_237 = new cjs.Shape();
            this.shape_237.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_237.setTransform(190.1216,-276.3472,0.7277,0.3104,-90);

            this.shape_238 = new cjs.Shape();
            this.shape_238.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_238.setTransform(190.2473,-338.3784,0.7277,0.3104);

            this.shape_239 = new cjs.Shape();
            this.shape_239.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_239.setTransform(190.1216,-338.1972,0.7277,0.3104,-90);

            this.shape_240 = new cjs.Shape();
            this.shape_240.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_240.setTransform(144.8973,338.3716,0.7277,0.3104);

            this.shape_241 = new cjs.Shape();
            this.shape_241.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_241.setTransform(145.0716,339.1528,0.7277,0.3104,-90);

            this.shape_242 = new cjs.Shape();
            this.shape_242.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_242.setTransform(144.8973,276.8716,0.7277,0.3104);

            this.shape_243 = new cjs.Shape();
            this.shape_243.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_243.setTransform(145.0716,277.4028,0.7277,0.3104,-90);

            this.shape_244 = new cjs.Shape();
            this.shape_244.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_244.setTransform(144.8973,217.4216,0.7277,0.3104);

            this.shape_245 = new cjs.Shape();
            this.shape_245.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_245.setTransform(145.0716,218.1028,0.7277,0.3104,-90);

            this.shape_246 = new cjs.Shape();
            this.shape_246.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_246.setTransform(144.8973,155.5716,0.7277,0.3104);

            this.shape_247 = new cjs.Shape();
            this.shape_247.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_247.setTransform(145.0716,156.0028,0.7277,0.3104,-90);

            this.shape_248 = new cjs.Shape();
            this.shape_248.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_248.setTransform(144.8973,87.5716,0.7277,0.3104);

            this.shape_249 = new cjs.Shape();
            this.shape_249.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_249.setTransform(145.0716,88.0028,0.7277,0.3104,-90);

            this.shape_250 = new cjs.Shape();
            this.shape_250.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_250.setTransform(144.8973,25.9716,0.7277,0.3104);

            this.shape_251 = new cjs.Shape();
            this.shape_251.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_251.setTransform(145.0716,26.1028,0.7277,0.3104,-90);

            this.shape_252 = new cjs.Shape();
            this.shape_252.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_252.setTransform(144.8973,-34.2284,0.7277,0.3104);

            this.shape_253 = new cjs.Shape();
            this.shape_253.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_253.setTransform(145.0716,-33.0972,0.7277,0.3104,-90);

            this.shape_254 = new cjs.Shape();
            this.shape_254.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_254.setTransform(144.8973,-95.1784,0.7277,0.3104);

            this.shape_255 = new cjs.Shape();
            this.shape_255.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_255.setTransform(145.0716,-94.6472,0.7277,0.3104,-90);

            this.shape_256 = new cjs.Shape();
            this.shape_256.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_256.setTransform(144.8973,-154.9284,0.7277,0.3104);

            this.shape_257 = new cjs.Shape();
            this.shape_257.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_257.setTransform(145.0716,-154.5972,0.7277,0.3104,-90);

            this.shape_258 = new cjs.Shape();
            this.shape_258.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_258.setTransform(144.8973,-216.7284,0.7277,0.3104);

            this.shape_259 = new cjs.Shape();
            this.shape_259.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_259.setTransform(145.0716,-216.0472,0.7277,0.3104,-90);

            this.shape_260 = new cjs.Shape();
            this.shape_260.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_260.setTransform(144.8973,-277.0284,0.7277,0.3104);

            this.shape_261 = new cjs.Shape();
            this.shape_261.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_261.setTransform(145.0716,-276.3472,0.7277,0.3104,-90);

            this.shape_262 = new cjs.Shape();
            this.shape_262.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_262.setTransform(144.8973,-338.3784,0.7277,0.3104);

            this.shape_263 = new cjs.Shape();
            this.shape_263.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_263.setTransform(145.0716,-338.1972,0.7277,0.3104,-90);

            this.shape_264 = new cjs.Shape();
            this.shape_264.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_264.setTransform(93.3972,338.3716,0.7277,0.3104);

            this.shape_265 = new cjs.Shape();
            this.shape_265.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_265.setTransform(93.6716,339.1528,0.7277,0.3104,-90);

            this.shape_266 = new cjs.Shape();
            this.shape_266.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_266.setTransform(93.3972,276.8716,0.7277,0.3104);

            this.shape_267 = new cjs.Shape();
            this.shape_267.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_267.setTransform(93.6716,277.4028,0.7277,0.3104,-90);

            this.shape_268 = new cjs.Shape();
            this.shape_268.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_268.setTransform(93.3972,217.4216,0.7277,0.3104);

            this.shape_269 = new cjs.Shape();
            this.shape_269.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_269.setTransform(93.6716,218.1028,0.7277,0.3104,-90);

            this.shape_270 = new cjs.Shape();
            this.shape_270.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_270.setTransform(93.3972,155.5716,0.7277,0.3104);

            this.shape_271 = new cjs.Shape();
            this.shape_271.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_271.setTransform(93.6716,156.0028,0.7277,0.3104,-90);

            this.shape_272 = new cjs.Shape();
            this.shape_272.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_272.setTransform(93.3972,87.5716,0.7277,0.3104);

            this.shape_273 = new cjs.Shape();
            this.shape_273.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_273.setTransform(93.6716,88.0028,0.7277,0.3104,-90);

            this.shape_274 = new cjs.Shape();
            this.shape_274.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_274.setTransform(93.3972,25.9716,0.7277,0.3104);

            this.shape_275 = new cjs.Shape();
            this.shape_275.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_275.setTransform(93.6716,26.1028,0.7277,0.3104,-90);

            this.shape_276 = new cjs.Shape();
            this.shape_276.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_276.setTransform(93.3972,-34.2284,0.7277,0.3104);

            this.shape_277 = new cjs.Shape();
            this.shape_277.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_277.setTransform(93.6716,-33.0972,0.7277,0.3104,-90);

            this.shape_278 = new cjs.Shape();
            this.shape_278.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_278.setTransform(93.3972,-95.1784,0.7277,0.3104);

            this.shape_279 = new cjs.Shape();
            this.shape_279.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_279.setTransform(93.6716,-94.6472,0.7277,0.3104,-90);

            this.shape_280 = new cjs.Shape();
            this.shape_280.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_280.setTransform(93.3972,-154.9284,0.7277,0.3104);

            this.shape_281 = new cjs.Shape();
            this.shape_281.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_281.setTransform(93.6716,-154.5972,0.7277,0.3104,-90);

            this.shape_282 = new cjs.Shape();
            this.shape_282.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_282.setTransform(93.3972,-216.7284,0.7277,0.3104);

            this.shape_283 = new cjs.Shape();
            this.shape_283.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_283.setTransform(93.6716,-216.0472,0.7277,0.3104,-90);

            this.shape_284 = new cjs.Shape();
            this.shape_284.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_284.setTransform(93.3972,-277.0284,0.7277,0.3104);

            this.shape_285 = new cjs.Shape();
            this.shape_285.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_285.setTransform(93.6716,-276.3472,0.7277,0.3104,-90);

            this.shape_286 = new cjs.Shape();
            this.shape_286.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_286.setTransform(93.3972,-338.3784,0.7277,0.3104);

            this.shape_287 = new cjs.Shape();
            this.shape_287.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_287.setTransform(93.6716,-338.1972,0.7277,0.3104,-90);

            this.shape_288 = new cjs.Shape();
            this.shape_288.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_288.setTransform(47.3972,338.3716,0.7277,0.3104);

            this.shape_289 = new cjs.Shape();
            this.shape_289.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_289.setTransform(47.6716,339.1528,0.7277,0.3104,-90);

            this.shape_290 = new cjs.Shape();
            this.shape_290.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_290.setTransform(47.3972,276.8716,0.7277,0.3104);

            this.shape_291 = new cjs.Shape();
            this.shape_291.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_291.setTransform(47.6716,277.4028,0.7277,0.3104,-90);

            this.shape_292 = new cjs.Shape();
            this.shape_292.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_292.setTransform(47.3972,217.4216,0.7277,0.3104);

            this.shape_293 = new cjs.Shape();
            this.shape_293.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_293.setTransform(47.6716,218.1028,0.7277,0.3104,-90);

            this.shape_294 = new cjs.Shape();
            this.shape_294.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_294.setTransform(47.3972,155.5716,0.7277,0.3104);

            this.shape_295 = new cjs.Shape();
            this.shape_295.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_295.setTransform(47.6716,156.0028,0.7277,0.3104,-90);

            this.shape_296 = new cjs.Shape();
            this.shape_296.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_296.setTransform(47.3972,87.5716,0.7277,0.3104);

            this.shape_297 = new cjs.Shape();
            this.shape_297.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_297.setTransform(47.6716,88.0028,0.7277,0.3104,-90);

            this.shape_298 = new cjs.Shape();
            this.shape_298.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_298.setTransform(47.3972,25.9716,0.7277,0.3104);

            this.shape_299 = new cjs.Shape();
            this.shape_299.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_299.setTransform(47.6716,26.1028,0.7277,0.3104,-90);

            this.shape_300 = new cjs.Shape();
            this.shape_300.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_300.setTransform(47.3972,-34.2284,0.7277,0.3104);

            this.shape_301 = new cjs.Shape();
            this.shape_301.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_301.setTransform(47.6716,-33.0972,0.7277,0.3104,-90);

            this.shape_302 = new cjs.Shape();
            this.shape_302.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_302.setTransform(47.3972,-95.1784,0.7277,0.3104);

            this.shape_303 = new cjs.Shape();
            this.shape_303.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_303.setTransform(47.6716,-94.6472,0.7277,0.3104,-90);

            this.shape_304 = new cjs.Shape();
            this.shape_304.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_304.setTransform(47.3972,-154.9284,0.7277,0.3104);

            this.shape_305 = new cjs.Shape();
            this.shape_305.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_305.setTransform(47.6716,-154.5972,0.7277,0.3104,-90);

            this.shape_306 = new cjs.Shape();
            this.shape_306.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_306.setTransform(47.3972,-216.7284,0.7277,0.3104);

            this.shape_307 = new cjs.Shape();
            this.shape_307.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_307.setTransform(47.6716,-216.0472,0.7277,0.3104,-90);

            this.shape_308 = new cjs.Shape();
            this.shape_308.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_308.setTransform(47.3972,-277.0284,0.7277,0.3104);

            this.shape_309 = new cjs.Shape();
            this.shape_309.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_309.setTransform(47.6716,-276.3472,0.7277,0.3104,-90);

            this.shape_310 = new cjs.Shape();
            this.shape_310.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_310.setTransform(47.3972,-338.3784,0.7277,0.3104);

            this.shape_311 = new cjs.Shape();
            this.shape_311.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_311.setTransform(47.6716,-338.1972,0.7277,0.3104,-90);

            this.shape_312 = new cjs.Shape();
            this.shape_312.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_312.setTransform(-2.7028,338.3716,0.7277,0.3104);

            this.shape_313 = new cjs.Shape();
            this.shape_313.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_313.setTransform(-2.1284,339.1528,0.7277,0.3104,-90);

            this.shape_314 = new cjs.Shape();
            this.shape_314.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_314.setTransform(-2.7028,276.8716,0.7277,0.3104);

            this.shape_315 = new cjs.Shape();
            this.shape_315.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_315.setTransform(-2.1284,277.4028,0.7277,0.3104,-90);

            this.shape_316 = new cjs.Shape();
            this.shape_316.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_316.setTransform(-2.7028,217.4216,0.7277,0.3104);

            this.shape_317 = new cjs.Shape();
            this.shape_317.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_317.setTransform(-2.1284,218.1028,0.7277,0.3104,-90);

            this.shape_318 = new cjs.Shape();
            this.shape_318.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_318.setTransform(-2.7028,155.5716,0.7277,0.3104);

            this.shape_319 = new cjs.Shape();
            this.shape_319.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_319.setTransform(-2.1284,156.0028,0.7277,0.3104,-90);

            this.shape_320 = new cjs.Shape();
            this.shape_320.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_320.setTransform(-2.7028,87.5716,0.7277,0.3104);

            this.shape_321 = new cjs.Shape();
            this.shape_321.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_321.setTransform(-2.1284,88.0028,0.7277,0.3104,-90);

            this.shape_322 = new cjs.Shape();
            this.shape_322.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_322.setTransform(-2.7028,25.9716,0.7277,0.3104);

            this.shape_323 = new cjs.Shape();
            this.shape_323.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_323.setTransform(-2.1284,26.1028,0.7277,0.3104,-90);

            this.shape_324 = new cjs.Shape();
            this.shape_324.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_324.setTransform(-2.7028,-34.2284,0.7277,0.3104);

            this.shape_325 = new cjs.Shape();
            this.shape_325.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_325.setTransform(-2.1284,-33.0972,0.7277,0.3104,-90);

            this.shape_326 = new cjs.Shape();
            this.shape_326.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_326.setTransform(-2.7028,-95.1784,0.7277,0.3104);

            this.shape_327 = new cjs.Shape();
            this.shape_327.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_327.setTransform(-2.1284,-94.6472,0.7277,0.3104,-90);

            this.shape_328 = new cjs.Shape();
            this.shape_328.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_328.setTransform(-2.7028,-154.9284,0.7277,0.3104);

            this.shape_329 = new cjs.Shape();
            this.shape_329.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_329.setTransform(-2.1284,-154.5972,0.7277,0.3104,-90);

            this.shape_330 = new cjs.Shape();
            this.shape_330.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_330.setTransform(-2.7028,-216.7284,0.7277,0.3104);

            this.shape_331 = new cjs.Shape();
            this.shape_331.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_331.setTransform(-2.1284,-216.0472,0.7277,0.3104,-90);

            this.shape_332 = new cjs.Shape();
            this.shape_332.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_332.setTransform(-2.7028,-277.0284,0.7277,0.3104);

            this.shape_333 = new cjs.Shape();
            this.shape_333.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_333.setTransform(-2.1284,-276.3472,0.7277,0.3104,-90);

            this.shape_334 = new cjs.Shape();
            this.shape_334.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_334.setTransform(-2.7028,-338.3784,0.7277,0.3104);

            this.shape_335 = new cjs.Shape();
            this.shape_335.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_335.setTransform(-2.1284,-338.1972,0.7277,0.3104,-90);

            this.shape_336 = new cjs.Shape();
            this.shape_336.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_336.setTransform(-47.7528,338.3716,0.7277,0.3104);

            this.shape_337 = new cjs.Shape();
            this.shape_337.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_337.setTransform(-47.3784,339.1528,0.7277,0.3104,-90);

            this.shape_338 = new cjs.Shape();
            this.shape_338.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_338.setTransform(-47.7528,276.8716,0.7277,0.3104);

            this.shape_339 = new cjs.Shape();
            this.shape_339.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_339.setTransform(-47.3784,277.4028,0.7277,0.3104,-90);

            this.shape_340 = new cjs.Shape();
            this.shape_340.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_340.setTransform(-47.7528,217.4216,0.7277,0.3104);

            this.shape_341 = new cjs.Shape();
            this.shape_341.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_341.setTransform(-47.3784,218.1028,0.7277,0.3104,-90);

            this.shape_342 = new cjs.Shape();
            this.shape_342.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_342.setTransform(-47.7528,155.5716,0.7277,0.3104);

            this.shape_343 = new cjs.Shape();
            this.shape_343.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_343.setTransform(-47.3784,156.0028,0.7277,0.3104,-90);

            this.shape_344 = new cjs.Shape();
            this.shape_344.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_344.setTransform(-47.7528,87.5716,0.7277,0.3104);

            this.shape_345 = new cjs.Shape();
            this.shape_345.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_345.setTransform(-47.3784,88.0028,0.7277,0.3104,-90);

            this.shape_346 = new cjs.Shape();
            this.shape_346.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_346.setTransform(-47.7528,25.9716,0.7277,0.3104);

            this.shape_347 = new cjs.Shape();
            this.shape_347.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_347.setTransform(-47.3784,26.1028,0.7277,0.3104,-90);

            this.shape_348 = new cjs.Shape();
            this.shape_348.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_348.setTransform(-47.7528,-34.2284,0.7277,0.3104);

            this.shape_349 = new cjs.Shape();
            this.shape_349.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_349.setTransform(-47.3784,-33.0972,0.7277,0.3104,-90);

            this.shape_350 = new cjs.Shape();
            this.shape_350.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_350.setTransform(-47.7528,-95.1784,0.7277,0.3104);

            this.shape_351 = new cjs.Shape();
            this.shape_351.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_351.setTransform(-47.3784,-94.6472,0.7277,0.3104,-90);

            this.shape_352 = new cjs.Shape();
            this.shape_352.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_352.setTransform(-47.7528,-154.9284,0.7277,0.3104);

            this.shape_353 = new cjs.Shape();
            this.shape_353.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_353.setTransform(-47.3784,-154.5972,0.7277,0.3104,-90);

            this.shape_354 = new cjs.Shape();
            this.shape_354.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_354.setTransform(-47.7528,-216.7284,0.7277,0.3104);

            this.shape_355 = new cjs.Shape();
            this.shape_355.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_355.setTransform(-47.3784,-216.0472,0.7277,0.3104,-90);

            this.shape_356 = new cjs.Shape();
            this.shape_356.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_356.setTransform(-47.7528,-277.0284,0.7277,0.3104);

            this.shape_357 = new cjs.Shape();
            this.shape_357.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_357.setTransform(-47.3784,-276.3472,0.7277,0.3104,-90);

            this.shape_358 = new cjs.Shape();
            this.shape_358.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_358.setTransform(-47.7528,-338.3784,0.7277,0.3104);

            this.shape_359 = new cjs.Shape();
            this.shape_359.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_359.setTransform(-47.3784,-338.1972,0.7277,0.3104,-90);

            this.shape_360 = new cjs.Shape();
            this.shape_360.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_360.setTransform(-96.5028,338.3716,0.7277,0.3104);

            this.shape_361 = new cjs.Shape();
            this.shape_361.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_361.setTransform(-96.0784,339.1528,0.7277,0.3104,-90);

            this.shape_362 = new cjs.Shape();
            this.shape_362.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_362.setTransform(-96.5028,276.8716,0.7277,0.3104);

            this.shape_363 = new cjs.Shape();
            this.shape_363.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_363.setTransform(-96.0784,277.4028,0.7277,0.3104,-90);

            this.shape_364 = new cjs.Shape();
            this.shape_364.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_364.setTransform(-96.5028,217.4216,0.7277,0.3104);

            this.shape_365 = new cjs.Shape();
            this.shape_365.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_365.setTransform(-96.0784,218.1028,0.7277,0.3104,-90);

            this.shape_366 = new cjs.Shape();
            this.shape_366.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_366.setTransform(-96.5028,155.5716,0.7277,0.3104);

            this.shape_367 = new cjs.Shape();
            this.shape_367.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_367.setTransform(-96.0784,156.0028,0.7277,0.3104,-90);

            this.shape_368 = new cjs.Shape();
            this.shape_368.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_368.setTransform(-96.5028,87.5716,0.7277,0.3104);

            this.shape_369 = new cjs.Shape();
            this.shape_369.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_369.setTransform(-96.0784,88.0028,0.7277,0.3104,-90);

            this.shape_370 = new cjs.Shape();
            this.shape_370.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_370.setTransform(-96.5028,25.9716,0.7277,0.3104);

            this.shape_371 = new cjs.Shape();
            this.shape_371.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_371.setTransform(-96.0784,26.1028,0.7277,0.3104,-90);

            this.shape_372 = new cjs.Shape();
            this.shape_372.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_372.setTransform(-96.5028,-34.2284,0.7277,0.3104);

            this.shape_373 = new cjs.Shape();
            this.shape_373.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_373.setTransform(-96.0784,-33.0972,0.7277,0.3104,-90);

            this.shape_374 = new cjs.Shape();
            this.shape_374.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_374.setTransform(-96.5028,-95.1784,0.7277,0.3104);

            this.shape_375 = new cjs.Shape();
            this.shape_375.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_375.setTransform(-96.0784,-94.6472,0.7277,0.3104,-90);

            this.shape_376 = new cjs.Shape();
            this.shape_376.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_376.setTransform(-96.5028,-154.9284,0.7277,0.3104);

            this.shape_377 = new cjs.Shape();
            this.shape_377.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_377.setTransform(-96.0784,-154.5972,0.7277,0.3104,-90);

            this.shape_378 = new cjs.Shape();
            this.shape_378.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_378.setTransform(-96.5028,-216.7284,0.7277,0.3104);

            this.shape_379 = new cjs.Shape();
            this.shape_379.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_379.setTransform(-96.0784,-216.0472,0.7277,0.3104,-90);

            this.shape_380 = new cjs.Shape();
            this.shape_380.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_380.setTransform(-96.5028,-277.0284,0.7277,0.3104);

            this.shape_381 = new cjs.Shape();
            this.shape_381.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_381.setTransform(-96.0784,-276.3472,0.7277,0.3104,-90);

            this.shape_382 = new cjs.Shape();
            this.shape_382.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_382.setTransform(-96.5028,-338.3784,0.7277,0.3104);

            this.shape_383 = new cjs.Shape();
            this.shape_383.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_383.setTransform(-96.0784,-338.1972,0.7277,0.3104,-90);

            this.shape_384 = new cjs.Shape();
            this.shape_384.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_384.setTransform(-141.5527,338.3716,0.7277,0.3104);

            this.shape_385 = new cjs.Shape();
            this.shape_385.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_385.setTransform(-141.5784,339.1528,0.7277,0.3104,-90);

            this.shape_386 = new cjs.Shape();
            this.shape_386.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_386.setTransform(-141.5527,276.8716,0.7277,0.3104);

            this.shape_387 = new cjs.Shape();
            this.shape_387.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_387.setTransform(-141.5784,277.4028,0.7277,0.3104,-90);

            this.shape_388 = new cjs.Shape();
            this.shape_388.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_388.setTransform(-141.5527,217.4216,0.7277,0.3104);

            this.shape_389 = new cjs.Shape();
            this.shape_389.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_389.setTransform(-141.5784,218.1028,0.7277,0.3104,-90);

            this.shape_390 = new cjs.Shape();
            this.shape_390.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_390.setTransform(-141.5527,155.5716,0.7277,0.3104);

            this.shape_391 = new cjs.Shape();
            this.shape_391.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_391.setTransform(-141.5784,156.0028,0.7277,0.3104,-90);

            this.shape_392 = new cjs.Shape();
            this.shape_392.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_392.setTransform(-141.5527,87.5716,0.7277,0.3104);

            this.shape_393 = new cjs.Shape();
            this.shape_393.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_393.setTransform(-141.5784,88.0028,0.7277,0.3104,-90);

            this.shape_394 = new cjs.Shape();
            this.shape_394.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_394.setTransform(-141.5527,25.9716,0.7277,0.3104);

            this.shape_395 = new cjs.Shape();
            this.shape_395.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_395.setTransform(-141.5784,26.1028,0.7277,0.3104,-90);

            this.shape_396 = new cjs.Shape();
            this.shape_396.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_396.setTransform(-141.5527,-34.2284,0.7277,0.3104);

            this.shape_397 = new cjs.Shape();
            this.shape_397.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_397.setTransform(-141.5784,-33.0972,0.7277,0.3104,-90);

            this.shape_398 = new cjs.Shape();
            this.shape_398.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_398.setTransform(-141.5527,-95.1784,0.7277,0.3104);

            this.shape_399 = new cjs.Shape();
            this.shape_399.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_399.setTransform(-141.5784,-94.6472,0.7277,0.3104,-90);

            this.shape_400 = new cjs.Shape();
            this.shape_400.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_400.setTransform(-141.5527,-154.9284,0.7277,0.3104);

            this.shape_401 = new cjs.Shape();
            this.shape_401.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_401.setTransform(-141.5784,-154.5972,0.7277,0.3104,-90);

            this.shape_402 = new cjs.Shape();
            this.shape_402.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_402.setTransform(-141.5527,-216.7284,0.7277,0.3104);

            this.shape_403 = new cjs.Shape();
            this.shape_403.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_403.setTransform(-141.5784,-216.0472,0.7277,0.3104,-90);

            this.shape_404 = new cjs.Shape();
            this.shape_404.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_404.setTransform(-141.5527,-277.0284,0.7277,0.3104);

            this.shape_405 = new cjs.Shape();
            this.shape_405.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_405.setTransform(-141.5784,-276.3472,0.7277,0.3104,-90);

            this.shape_406 = new cjs.Shape();
            this.shape_406.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_406.setTransform(-141.5527,-338.3784,0.7277,0.3104);

            this.shape_407 = new cjs.Shape();
            this.shape_407.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_407.setTransform(-141.5784,-338.1972,0.7277,0.3104,-90);

            this.shape_408 = new cjs.Shape();
            this.shape_408.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_408.setTransform(-191.8027,338.3716,0.7277,0.3104);

            this.shape_409 = new cjs.Shape();
            this.shape_409.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_409.setTransform(-191.9284,339.1528,0.7277,0.3104,-90);

            this.shape_410 = new cjs.Shape();
            this.shape_410.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_410.setTransform(-191.8027,276.8716,0.7277,0.3104);

            this.shape_411 = new cjs.Shape();
            this.shape_411.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_411.setTransform(-191.9284,277.4028,0.7277,0.3104,-90);

            this.shape_412 = new cjs.Shape();
            this.shape_412.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_412.setTransform(-191.8027,217.4216,0.7277,0.3104);

            this.shape_413 = new cjs.Shape();
            this.shape_413.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_413.setTransform(-191.9284,218.1028,0.7277,0.3104,-90);

            this.shape_414 = new cjs.Shape();
            this.shape_414.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_414.setTransform(-191.8027,155.5716,0.7277,0.3104);

            this.shape_415 = new cjs.Shape();
            this.shape_415.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_415.setTransform(-191.9284,156.0028,0.7277,0.3104,-90);

            this.shape_416 = new cjs.Shape();
            this.shape_416.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_416.setTransform(-191.8027,87.5716,0.7277,0.3104);

            this.shape_417 = new cjs.Shape();
            this.shape_417.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_417.setTransform(-191.9284,88.0028,0.7277,0.3104,-90);

            this.shape_418 = new cjs.Shape();
            this.shape_418.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_418.setTransform(-191.8027,25.9716,0.7277,0.3104);

            this.shape_419 = new cjs.Shape();
            this.shape_419.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_419.setTransform(-191.9284,26.1028,0.7277,0.3104,-90);

            this.shape_420 = new cjs.Shape();
            this.shape_420.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_420.setTransform(-191.8027,-34.2284,0.7277,0.3104);

            this.shape_421 = new cjs.Shape();
            this.shape_421.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_421.setTransform(-191.9284,-33.0972,0.7277,0.3104,-90);

            this.shape_422 = new cjs.Shape();
            this.shape_422.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_422.setTransform(-191.8027,-95.1784,0.7277,0.3104);

            this.shape_423 = new cjs.Shape();
            this.shape_423.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_423.setTransform(-191.9284,-94.6472,0.7277,0.3104,-90);

            this.shape_424 = new cjs.Shape();
            this.shape_424.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_424.setTransform(-191.8027,-154.9284,0.7277,0.3104);

            this.shape_425 = new cjs.Shape();
            this.shape_425.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_425.setTransform(-191.9284,-154.5972,0.7277,0.3104,-90);

            this.shape_426 = new cjs.Shape();
            this.shape_426.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_426.setTransform(-191.8027,-216.7284,0.7277,0.3104);

            this.shape_427 = new cjs.Shape();
            this.shape_427.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_427.setTransform(-191.9284,-216.0472,0.7277,0.3104,-90);

            this.shape_428 = new cjs.Shape();
            this.shape_428.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_428.setTransform(-191.8027,-277.0284,0.7277,0.3104);

            this.shape_429 = new cjs.Shape();
            this.shape_429.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_429.setTransform(-191.9284,-276.3472,0.7277,0.3104,-90);

            this.shape_430 = new cjs.Shape();
            this.shape_430.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_430.setTransform(-191.8027,-338.3784,0.7277,0.3104);

            this.shape_431 = new cjs.Shape();
            this.shape_431.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_431.setTransform(-191.9284,-338.1972,0.7277,0.3104,-90);

            this.shape_432 = new cjs.Shape();
            this.shape_432.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_432.setTransform(-237.3527,338.3716,0.7277,0.3104);

            this.shape_433 = new cjs.Shape();
            this.shape_433.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_433.setTransform(-236.9284,339.1528,0.7277,0.3104,-90);

            this.shape_434 = new cjs.Shape();
            this.shape_434.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_434.setTransform(-237.3527,276.8716,0.7277,0.3104);

            this.shape_435 = new cjs.Shape();
            this.shape_435.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_435.setTransform(-236.9284,277.4028,0.7277,0.3104,-90);

            this.shape_436 = new cjs.Shape();
            this.shape_436.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_436.setTransform(-237.3527,217.4216,0.7277,0.3104);

            this.shape_437 = new cjs.Shape();
            this.shape_437.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_437.setTransform(-236.9284,218.1028,0.7277,0.3104,-90);

            this.shape_438 = new cjs.Shape();
            this.shape_438.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_438.setTransform(-237.3527,155.5716,0.7277,0.3104);

            this.shape_439 = new cjs.Shape();
            this.shape_439.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_439.setTransform(-236.9284,156.0028,0.7277,0.3104,-90);

            this.shape_440 = new cjs.Shape();
            this.shape_440.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_440.setTransform(-237.3527,87.5716,0.7277,0.3104);

            this.shape_441 = new cjs.Shape();
            this.shape_441.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_441.setTransform(-236.9284,88.0028,0.7277,0.3104,-90);

            this.shape_442 = new cjs.Shape();
            this.shape_442.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_442.setTransform(-237.3527,25.9716,0.7277,0.3104);

            this.shape_443 = new cjs.Shape();
            this.shape_443.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_443.setTransform(-236.9284,26.1028,0.7277,0.3104,-90);

            this.shape_444 = new cjs.Shape();
            this.shape_444.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_444.setTransform(-237.3527,-34.2284,0.7277,0.3104);

            this.shape_445 = new cjs.Shape();
            this.shape_445.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_445.setTransform(-236.9284,-33.0972,0.7277,0.3104,-90);

            this.shape_446 = new cjs.Shape();
            this.shape_446.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_446.setTransform(-237.3527,-95.1784,0.7277,0.3104);

            this.shape_447 = new cjs.Shape();
            this.shape_447.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_447.setTransform(-236.9284,-94.6472,0.7277,0.3104,-90);

            this.shape_448 = new cjs.Shape();
            this.shape_448.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_448.setTransform(-237.3527,-154.9284,0.7277,0.3104);

            this.shape_449 = new cjs.Shape();
            this.shape_449.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_449.setTransform(-236.9284,-154.5972,0.7277,0.3104,-90);

            this.shape_450 = new cjs.Shape();
            this.shape_450.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_450.setTransform(-237.3527,-216.7284,0.7277,0.3104);

            this.shape_451 = new cjs.Shape();
            this.shape_451.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_451.setTransform(-236.9284,-216.0472,0.7277,0.3104,-90);

            this.shape_452 = new cjs.Shape();
            this.shape_452.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_452.setTransform(-237.3527,-277.0284,0.7277,0.3104);

            this.shape_453 = new cjs.Shape();
            this.shape_453.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_453.setTransform(-236.9284,-276.3472,0.7277,0.3104,-90);

            this.shape_454 = new cjs.Shape();
            this.shape_454.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_454.setTransform(-237.3527,-338.3784,0.7277,0.3104);

            this.shape_455 = new cjs.Shape();
            this.shape_455.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_455.setTransform(-236.9284,-338.1972,0.7277,0.3104,-90);

            this.shape_456 = new cjs.Shape();
            this.shape_456.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_456.setTransform(-287.5027,338.3716,0.7277,0.3104);

            this.shape_457 = new cjs.Shape();
            this.shape_457.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_457.setTransform(-287.5284,339.1528,0.7277,0.3104,-90);

            this.shape_458 = new cjs.Shape();
            this.shape_458.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_458.setTransform(-287.5027,276.8716,0.7277,0.3104);

            this.shape_459 = new cjs.Shape();
            this.shape_459.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_459.setTransform(-287.5284,277.4028,0.7277,0.3104,-90);

            this.shape_460 = new cjs.Shape();
            this.shape_460.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_460.setTransform(-287.5027,217.4216,0.7277,0.3104);

            this.shape_461 = new cjs.Shape();
            this.shape_461.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_461.setTransform(-287.5284,218.1028,0.7277,0.3104,-90);

            this.shape_462 = new cjs.Shape();
            this.shape_462.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_462.setTransform(-287.5027,155.5716,0.7277,0.3104);

            this.shape_463 = new cjs.Shape();
            this.shape_463.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_463.setTransform(-287.5284,156.0028,0.7277,0.3104,-90);

            this.shape_464 = new cjs.Shape();
            this.shape_464.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_464.setTransform(-287.5027,87.5716,0.7277,0.3104);

            this.shape_465 = new cjs.Shape();
            this.shape_465.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_465.setTransform(-287.5284,88.0028,0.7277,0.3104,-90);

            this.shape_466 = new cjs.Shape();
            this.shape_466.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_466.setTransform(-287.5027,25.9716,0.7277,0.3104);

            this.shape_467 = new cjs.Shape();
            this.shape_467.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_467.setTransform(-287.5284,26.1028,0.7277,0.3104,-90);

            this.shape_468 = new cjs.Shape();
            this.shape_468.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_468.setTransform(-287.5027,-34.2284,0.7277,0.3104);

            this.shape_469 = new cjs.Shape();
            this.shape_469.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_469.setTransform(-287.5284,-33.0972,0.7277,0.3104,-90);

            this.shape_470 = new cjs.Shape();
            this.shape_470.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_470.setTransform(-287.5027,-95.1784,0.7277,0.3104);

            this.shape_471 = new cjs.Shape();
            this.shape_471.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_471.setTransform(-287.5284,-94.6472,0.7277,0.3104,-90);

            this.shape_472 = new cjs.Shape();
            this.shape_472.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_472.setTransform(-287.5027,-154.9284,0.7277,0.3104);

            this.shape_473 = new cjs.Shape();
            this.shape_473.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_473.setTransform(-287.5284,-154.5972,0.7277,0.3104,-90);

            this.shape_474 = new cjs.Shape();
            this.shape_474.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_474.setTransform(-287.5027,-216.7284,0.7277,0.3104);

            this.shape_475 = new cjs.Shape();
            this.shape_475.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_475.setTransform(-287.5284,-216.0472,0.7277,0.3104,-90);

            this.shape_476 = new cjs.Shape();
            this.shape_476.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_476.setTransform(-287.5027,-277.0284,0.7277,0.3104);

            this.shape_477 = new cjs.Shape();
            this.shape_477.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_477.setTransform(-287.5284,-276.3472,0.7277,0.3104,-90);

            this.shape_478 = new cjs.Shape();
            this.shape_478.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_478.setTransform(-287.5027,-338.3784,0.7277,0.3104);

            this.shape_479 = new cjs.Shape();
            this.shape_479.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_479.setTransform(-287.5284,-338.1972,0.7277,0.3104,-90);

            this.shape_480 = new cjs.Shape();
            this.shape_480.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_480.setTransform(-332.8027,338.3716,0.7277,0.3104);

            this.shape_481 = new cjs.Shape();
            this.shape_481.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_481.setTransform(-332.6784,339.1528,0.7277,0.3104,-90);

            this.shape_482 = new cjs.Shape();
            this.shape_482.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_482.setTransform(-332.8027,276.8716,0.7277,0.3104);

            this.shape_483 = new cjs.Shape();
            this.shape_483.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_483.setTransform(-332.6784,277.4028,0.7277,0.3104,-90);

            this.shape_484 = new cjs.Shape();
            this.shape_484.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_484.setTransform(-332.8027,217.4216,0.7277,0.3104);

            this.shape_485 = new cjs.Shape();
            this.shape_485.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_485.setTransform(-332.6784,218.1028,0.7277,0.3104,-90);

            this.shape_486 = new cjs.Shape();
            this.shape_486.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_486.setTransform(-332.8027,155.5716,0.7277,0.3104);

            this.shape_487 = new cjs.Shape();
            this.shape_487.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_487.setTransform(-332.6784,156.0028,0.7277,0.3104,-90);

            this.shape_488 = new cjs.Shape();
            this.shape_488.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_488.setTransform(-332.8027,87.5716,0.7277,0.3104);

            this.shape_489 = new cjs.Shape();
            this.shape_489.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_489.setTransform(-332.6784,88.0028,0.7277,0.3104,-90);

            this.shape_490 = new cjs.Shape();
            this.shape_490.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_490.setTransform(-332.8027,25.9716,0.7277,0.3104);

            this.shape_491 = new cjs.Shape();
            this.shape_491.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_491.setTransform(-332.6784,26.1028,0.7277,0.3104,-90);

            this.shape_492 = new cjs.Shape();
            this.shape_492.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_492.setTransform(-332.8027,-34.2284,0.7277,0.3104);

            this.shape_493 = new cjs.Shape();
            this.shape_493.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_493.setTransform(-332.6784,-33.0972,0.7277,0.3104,-90);

            this.shape_494 = new cjs.Shape();
            this.shape_494.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_494.setTransform(-332.8027,-95.1784,0.7277,0.3104);

            this.shape_495 = new cjs.Shape();
            this.shape_495.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_495.setTransform(-332.6784,-94.6472,0.7277,0.3104,-90);

            this.shape_496 = new cjs.Shape();
            this.shape_496.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_496.setTransform(-332.8027,-154.9284,0.7277,0.3104);

            this.shape_497 = new cjs.Shape();
            this.shape_497.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_497.setTransform(-332.6784,-154.5972,0.7277,0.3104,-90);

            this.shape_498 = new cjs.Shape();
            this.shape_498.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_498.setTransform(-332.8027,-216.7284,0.7277,0.3104);

            this.shape_499 = new cjs.Shape();
            this.shape_499.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_499.setTransform(-332.6784,-216.0472,0.7277,0.3104,-90);

            this.shape_500 = new cjs.Shape();
            this.shape_500.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_500.setTransform(-332.8027,-277.0284,0.7277,0.3104);

            this.shape_501 = new cjs.Shape();
            this.shape_501.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_501.setTransform(-332.6784,-276.3472,0.7277,0.3104,-90);

            this.shape_502 = new cjs.Shape();
            this.shape_502.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_502.setTransform(-332.8027,-338.3784,0.7277,0.3104);

            this.shape_503 = new cjs.Shape();
            this.shape_503.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_503.setTransform(-332.6784,-338.1972,0.7277,0.3104,-90);

            this.shape_504 = new cjs.Shape();
            this.shape_504.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_504.setTransform(-382.5527,338.3716,0.7277,0.3104);

            this.shape_505 = new cjs.Shape();
            this.shape_505.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_505.setTransform(-383.0784,339.1528,0.7277,0.3104,-90);

            this.shape_506 = new cjs.Shape();
            this.shape_506.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_506.setTransform(-382.5527,276.8716,0.7277,0.3104);

            this.shape_507 = new cjs.Shape();
            this.shape_507.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_507.setTransform(-383.0784,277.4028,0.7277,0.3104,-90);

            this.shape_508 = new cjs.Shape();
            this.shape_508.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_508.setTransform(-382.5527,217.4216,0.7277,0.3104);

            this.shape_509 = new cjs.Shape();
            this.shape_509.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_509.setTransform(-383.0784,218.1028,0.7277,0.3104,-90);

            this.shape_510 = new cjs.Shape();
            this.shape_510.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_510.setTransform(-382.5527,155.5716,0.7277,0.3104);

            this.shape_511 = new cjs.Shape();
            this.shape_511.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_511.setTransform(-383.0784,156.0028,0.7277,0.3104,-90);

            this.shape_512 = new cjs.Shape();
            this.shape_512.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_512.setTransform(-382.5527,87.5716,0.7277,0.3104);

            this.shape_513 = new cjs.Shape();
            this.shape_513.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_513.setTransform(-383.0784,88.0028,0.7277,0.3104,-90);

            this.shape_514 = new cjs.Shape();
            this.shape_514.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_514.setTransform(-382.5527,25.9716,0.7277,0.3104);

            this.shape_515 = new cjs.Shape();
            this.shape_515.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_515.setTransform(-383.0784,26.1028,0.7277,0.3104,-90);

            this.shape_516 = new cjs.Shape();
            this.shape_516.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_516.setTransform(-382.5527,-34.2284,0.7277,0.3104);

            this.shape_517 = new cjs.Shape();
            this.shape_517.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_517.setTransform(-383.0784,-33.0972,0.7277,0.3104,-90);

            this.shape_518 = new cjs.Shape();
            this.shape_518.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_518.setTransform(-382.5527,-95.1784,0.7277,0.3104);

            this.shape_519 = new cjs.Shape();
            this.shape_519.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_519.setTransform(-383.0784,-94.6472,0.7277,0.3104,-90);

            this.shape_520 = new cjs.Shape();
            this.shape_520.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_520.setTransform(-382.5527,-154.9284,0.7277,0.3104);

            this.shape_521 = new cjs.Shape();
            this.shape_521.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_521.setTransform(-383.0784,-154.5972,0.7277,0.3104,-90);

            this.shape_522 = new cjs.Shape();
            this.shape_522.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_522.setTransform(-382.5527,-216.7284,0.7277,0.3104);

            this.shape_523 = new cjs.Shape();
            this.shape_523.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_523.setTransform(-383.0784,-216.0472,0.7277,0.3104,-90);

            this.shape_524 = new cjs.Shape();
            this.shape_524.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_524.setTransform(-382.5527,-277.0284,0.7277,0.3104);

            this.shape_525 = new cjs.Shape();
            this.shape_525.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_525.setTransform(-383.0784,-276.3472,0.7277,0.3104,-90);

            this.shape_526 = new cjs.Shape();
            this.shape_526.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_526.setTransform(-382.5527,-338.3784,0.7277,0.3104);

            this.shape_527 = new cjs.Shape();
            this.shape_527.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_527.setTransform(-383.0784,-338.1972,0.7277,0.3104,-90);

            this.shape_528 = new cjs.Shape();
            this.shape_528.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_528.setTransform(-428.1527,338.3716,0.7277,0.3104);

            this.shape_529 = new cjs.Shape();
            this.shape_529.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_529.setTransform(-427.9784,339.1528,0.7277,0.3104,-90);

            this.shape_530 = new cjs.Shape();
            this.shape_530.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_530.setTransform(-428.1527,276.8716,0.7277,0.3104);

            this.shape_531 = new cjs.Shape();
            this.shape_531.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_531.setTransform(-427.9784,277.4028,0.7277,0.3104,-90);

            this.shape_532 = new cjs.Shape();
            this.shape_532.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_532.setTransform(-428.1527,217.4216,0.7277,0.3104);

            this.shape_533 = new cjs.Shape();
            this.shape_533.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_533.setTransform(-427.9784,218.1028,0.7277,0.3104,-90);

            this.shape_534 = new cjs.Shape();
            this.shape_534.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_534.setTransform(-428.1527,155.5716,0.7277,0.3104);

            this.shape_535 = new cjs.Shape();
            this.shape_535.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_535.setTransform(-427.9784,156.0028,0.7277,0.3104,-90);

            this.shape_536 = new cjs.Shape();
            this.shape_536.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_536.setTransform(-428.1527,87.5716,0.7277,0.3104);

            this.shape_537 = new cjs.Shape();
            this.shape_537.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_537.setTransform(-427.9784,88.0028,0.7277,0.3104,-90);

            this.shape_538 = new cjs.Shape();
            this.shape_538.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_538.setTransform(-428.1527,25.9716,0.7277,0.3104);

            this.shape_539 = new cjs.Shape();
            this.shape_539.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_539.setTransform(-427.9784,26.1028,0.7277,0.3104,-90);

            this.shape_540 = new cjs.Shape();
            this.shape_540.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_540.setTransform(-428.1527,-34.2284,0.7277,0.3104);

            this.shape_541 = new cjs.Shape();
            this.shape_541.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_541.setTransform(-427.9784,-33.0972,0.7277,0.3104,-90);

            this.shape_542 = new cjs.Shape();
            this.shape_542.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_542.setTransform(-428.1527,-95.1784,0.7277,0.3104);

            this.shape_543 = new cjs.Shape();
            this.shape_543.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_543.setTransform(-427.9784,-94.6472,0.7277,0.3104,-90);

            this.shape_544 = new cjs.Shape();
            this.shape_544.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_544.setTransform(-428.1527,-154.9284,0.7277,0.3104);

            this.shape_545 = new cjs.Shape();
            this.shape_545.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_545.setTransform(-427.9784,-154.5972,0.7277,0.3104,-90);

            this.shape_546 = new cjs.Shape();
            this.shape_546.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_546.setTransform(-428.1527,-216.7284,0.7277,0.3104);

            this.shape_547 = new cjs.Shape();
            this.shape_547.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_547.setTransform(-427.9784,-216.0472,0.7277,0.3104,-90);

            this.shape_548 = new cjs.Shape();
            this.shape_548.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_548.setTransform(-428.1527,-277.0284,0.7277,0.3104);

            this.shape_549 = new cjs.Shape();
            this.shape_549.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_549.setTransform(-427.9784,-276.3472,0.7277,0.3104,-90);

            this.shape_550 = new cjs.Shape();
            this.shape_550.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_550.setTransform(-428.1527,-338.3784,0.7277,0.3104);

            this.shape_551 = new cjs.Shape();
            this.shape_551.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_551.setTransform(-427.9784,-338.1972,0.7277,0.3104,-90);

            this.shape_552 = new cjs.Shape();
            this.shape_552.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_552.setTransform(-476.7027,338.3716,0.7277,0.3104);

            this.shape_553 = new cjs.Shape();
            this.shape_553.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_553.setTransform(-476.9784,339.1528,0.7277,0.3104,-90);

            this.shape_554 = new cjs.Shape();
            this.shape_554.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_554.setTransform(-476.7027,276.8716,0.7277,0.3104);

            this.shape_555 = new cjs.Shape();
            this.shape_555.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_555.setTransform(-476.9784,277.4028,0.7277,0.3104,-90);

            this.shape_556 = new cjs.Shape();
            this.shape_556.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_556.setTransform(-476.7027,217.4216,0.7277,0.3104);

            this.shape_557 = new cjs.Shape();
            this.shape_557.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_557.setTransform(-476.9784,218.1028,0.7277,0.3104,-90);

            this.shape_558 = new cjs.Shape();
            this.shape_558.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_558.setTransform(-476.7027,155.5716,0.7277,0.3104);

            this.shape_559 = new cjs.Shape();
            this.shape_559.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_559.setTransform(-476.9784,156.0028,0.7277,0.3104,-90);

            this.shape_560 = new cjs.Shape();
            this.shape_560.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_560.setTransform(-476.7027,87.5716,0.7277,0.3104);

            this.shape_561 = new cjs.Shape();
            this.shape_561.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_561.setTransform(-476.9784,88.0028,0.7277,0.3104,-90);

            this.shape_562 = new cjs.Shape();
            this.shape_562.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_562.setTransform(-476.7027,25.9716,0.7277,0.3104);

            this.shape_563 = new cjs.Shape();
            this.shape_563.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_563.setTransform(-476.9784,26.1028,0.7277,0.3104,-90);

            this.shape_564 = new cjs.Shape();
            this.shape_564.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_564.setTransform(-476.7027,-34.2284,0.7277,0.3104);

            this.shape_565 = new cjs.Shape();
            this.shape_565.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_565.setTransform(-476.9784,-33.0972,0.7277,0.3104,-90);

            this.shape_566 = new cjs.Shape();
            this.shape_566.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_566.setTransform(-476.7027,-95.1784,0.7277,0.3104);

            this.shape_567 = new cjs.Shape();
            this.shape_567.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_567.setTransform(-476.9784,-94.6472,0.7277,0.3104,-90);

            this.shape_568 = new cjs.Shape();
            this.shape_568.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_568.setTransform(-476.7027,-154.9284,0.7277,0.3104);

            this.shape_569 = new cjs.Shape();
            this.shape_569.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_569.setTransform(-476.9784,-154.5972,0.7277,0.3104,-90);

            this.shape_570 = new cjs.Shape();
            this.shape_570.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_570.setTransform(-476.7027,-216.7284,0.7277,0.3104);

            this.shape_571 = new cjs.Shape();
            this.shape_571.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_571.setTransform(-476.9784,-216.0472,0.7277,0.3104,-90);

            this.shape_572 = new cjs.Shape();
            this.shape_572.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_572.setTransform(-476.7027,-277.0284,0.7277,0.3104);

            this.shape_573 = new cjs.Shape();
            this.shape_573.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_573.setTransform(-476.9784,-276.3472,0.7277,0.3104,-90);

            this.shape_574 = new cjs.Shape();
            this.shape_574.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_574.setTransform(-476.7027,-338.3784,0.7277,0.3104);

            this.shape_575 = new cjs.Shape();
            this.shape_575.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_575.setTransform(-476.9784,-338.1972,0.7277,0.3104,-90);

            this.shape_576 = new cjs.Shape();
            this.shape_576.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_576.setTransform(-521.4027,338.3716,0.7277,0.3104);

            this.shape_577 = new cjs.Shape();
            this.shape_577.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_577.setTransform(-521.7284,339.1528,0.7277,0.3104,-90);

            this.shape_578 = new cjs.Shape();
            this.shape_578.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_578.setTransform(-521.4027,276.8716,0.7277,0.3104);

            this.shape_579 = new cjs.Shape();
            this.shape_579.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_579.setTransform(-521.7284,277.4028,0.7277,0.3104,-90);

            this.shape_580 = new cjs.Shape();
            this.shape_580.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_580.setTransform(-521.4027,217.4216,0.7277,0.3104);

            this.shape_581 = new cjs.Shape();
            this.shape_581.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_581.setTransform(-521.7284,218.1028,0.7277,0.3104,-90);

            this.shape_582 = new cjs.Shape();
            this.shape_582.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_582.setTransform(-521.4027,155.5716,0.7277,0.3104);

            this.shape_583 = new cjs.Shape();
            this.shape_583.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_583.setTransform(-521.7284,156.0028,0.7277,0.3104,-90);

            this.shape_584 = new cjs.Shape();
            this.shape_584.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_584.setTransform(-521.4027,87.5716,0.7277,0.3104);

            this.shape_585 = new cjs.Shape();
            this.shape_585.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_585.setTransform(-521.7284,88.0028,0.7277,0.3104,-90);

            this.shape_586 = new cjs.Shape();
            this.shape_586.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_586.setTransform(-521.4027,25.9716,0.7277,0.3104);

            this.shape_587 = new cjs.Shape();
            this.shape_587.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_587.setTransform(-521.7284,26.1028,0.7277,0.3104,-90);

            this.shape_588 = new cjs.Shape();
            this.shape_588.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_588.setTransform(-521.4027,-34.2284,0.7277,0.3104);

            this.shape_589 = new cjs.Shape();
            this.shape_589.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_589.setTransform(-521.7284,-33.0972,0.7277,0.3104,-90);

            this.shape_590 = new cjs.Shape();
            this.shape_590.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_590.setTransform(-521.4027,-95.1784,0.7277,0.3104);

            this.shape_591 = new cjs.Shape();
            this.shape_591.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_591.setTransform(-521.7284,-94.6472,0.7277,0.3104,-90);

            this.shape_592 = new cjs.Shape();
            this.shape_592.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_592.setTransform(-521.4027,-154.9284,0.7277,0.3104);

            this.shape_593 = new cjs.Shape();
            this.shape_593.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_593.setTransform(-521.7284,-154.5972,0.7277,0.3104,-90);

            this.shape_594 = new cjs.Shape();
            this.shape_594.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_594.setTransform(-521.4027,-216.7284,0.7277,0.3104);

            this.shape_595 = new cjs.Shape();
            this.shape_595.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_595.setTransform(-521.7284,-216.0472,0.7277,0.3104,-90);

            this.shape_596 = new cjs.Shape();
            this.shape_596.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_596.setTransform(-521.4027,-277.0284,0.7277,0.3104);

            this.shape_597 = new cjs.Shape();
            this.shape_597.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_597.setTransform(-521.7284,-276.3472,0.7277,0.3104,-90);

            this.shape_598 = new cjs.Shape();
            this.shape_598.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_598.setTransform(-521.4027,-338.3784,0.7277,0.3104);

            this.shape_599 = new cjs.Shape();
            this.shape_599.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_599.setTransform(-521.7284,-338.1972,0.7277,0.3104,-90);

            this.shape_600 = new cjs.Shape();
            this.shape_600.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_600.setTransform(-572.2027,338.3716,0.7277,0.3104);

            this.shape_601 = new cjs.Shape();
            this.shape_601.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_601.setTransform(-572.0284,339.1528,0.7277,0.3104,-90);

            this.shape_602 = new cjs.Shape();
            this.shape_602.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_602.setTransform(-572.2027,276.8716,0.7277,0.3104);

            this.shape_603 = new cjs.Shape();
            this.shape_603.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_603.setTransform(-572.0284,277.4028,0.7277,0.3104,-90);

            this.shape_604 = new cjs.Shape();
            this.shape_604.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_604.setTransform(-572.2027,217.4216,0.7277,0.3104);

            this.shape_605 = new cjs.Shape();
            this.shape_605.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_605.setTransform(-572.0284,218.1028,0.7277,0.3104,-90);

            this.shape_606 = new cjs.Shape();
            this.shape_606.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_606.setTransform(-572.2027,155.5716,0.7277,0.3104);

            this.shape_607 = new cjs.Shape();
            this.shape_607.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_607.setTransform(-572.0284,156.0028,0.7277,0.3104,-90);

            this.shape_608 = new cjs.Shape();
            this.shape_608.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_608.setTransform(-572.2027,87.5716,0.7277,0.3104);

            this.shape_609 = new cjs.Shape();
            this.shape_609.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_609.setTransform(-572.0284,88.0028,0.7277,0.3104,-90);

            this.shape_610 = new cjs.Shape();
            this.shape_610.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_610.setTransform(-572.2027,25.9716,0.7277,0.3104);

            this.shape_611 = new cjs.Shape();
            this.shape_611.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_611.setTransform(-572.0284,26.1028,0.7277,0.3104,-90);

            this.shape_612 = new cjs.Shape();
            this.shape_612.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_612.setTransform(-572.2027,-34.2284,0.7277,0.3104);

            this.shape_613 = new cjs.Shape();
            this.shape_613.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_613.setTransform(-572.0284,-33.0972,0.7277,0.3104,-90);

            this.shape_614 = new cjs.Shape();
            this.shape_614.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_614.setTransform(-572.2027,-95.1784,0.7277,0.3104);

            this.shape_615 = new cjs.Shape();
            this.shape_615.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_615.setTransform(-572.0284,-94.6472,0.7277,0.3104,-90);

            this.shape_616 = new cjs.Shape();
            this.shape_616.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_616.setTransform(-572.2027,-154.9284,0.7277,0.3104);

            this.shape_617 = new cjs.Shape();
            this.shape_617.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_617.setTransform(-572.0284,-154.5972,0.7277,0.3104,-90);

            this.shape_618 = new cjs.Shape();
            this.shape_618.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_618.setTransform(-572.2027,-216.7284,0.7277,0.3104);

            this.shape_619 = new cjs.Shape();
            this.shape_619.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_619.setTransform(-572.0284,-216.0472,0.7277,0.3104,-90);

            this.shape_620 = new cjs.Shape();
            this.shape_620.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_620.setTransform(-572.2027,-277.0284,0.7277,0.3104);

            this.shape_621 = new cjs.Shape();
            this.shape_621.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_621.setTransform(-572.0284,-276.3472,0.7277,0.3104,-90);

            this.shape_622 = new cjs.Shape();
            this.shape_622.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_622.setTransform(-572.2027,-338.3784,0.7277,0.3104);

            this.shape_623 = new cjs.Shape();
            this.shape_623.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_623.setTransform(-572.0284,-338.1972,0.7277,0.3104,-90);

            this.shape_624 = new cjs.Shape();
            this.shape_624.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_624.setTransform(-617.7027,338.3716,0.7277,0.3104);

            this.shape_625 = new cjs.Shape();
            this.shape_625.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_625.setTransform(-617.7784,339.1528,0.7277,0.3104,-90);

            this.shape_626 = new cjs.Shape();
            this.shape_626.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_626.setTransform(-617.7027,276.8716,0.7277,0.3104);

            this.shape_627 = new cjs.Shape();
            this.shape_627.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_627.setTransform(-617.7784,277.4028,0.7277,0.3104,-90);

            this.shape_628 = new cjs.Shape();
            this.shape_628.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_628.setTransform(-617.7027,217.4216,0.7277,0.3104);

            this.shape_629 = new cjs.Shape();
            this.shape_629.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_629.setTransform(-617.7784,218.1028,0.7277,0.3104,-90);

            this.shape_630 = new cjs.Shape();
            this.shape_630.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_630.setTransform(-617.7027,155.5716,0.7277,0.3104);

            this.shape_631 = new cjs.Shape();
            this.shape_631.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_631.setTransform(-617.7784,156.0028,0.7277,0.3104,-90);

            this.shape_632 = new cjs.Shape();
            this.shape_632.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_632.setTransform(-617.7027,87.5716,0.7277,0.3104);

            this.shape_633 = new cjs.Shape();
            this.shape_633.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_633.setTransform(-617.7784,88.0028,0.7277,0.3104,-90);

            this.shape_634 = new cjs.Shape();
            this.shape_634.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_634.setTransform(-617.7027,25.9716,0.7277,0.3104);

            this.shape_635 = new cjs.Shape();
            this.shape_635.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_635.setTransform(-617.7784,26.1028,0.7277,0.3104,-90);

            this.shape_636 = new cjs.Shape();
            this.shape_636.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_636.setTransform(-617.7027,-34.2284,0.7277,0.3104);

            this.shape_637 = new cjs.Shape();
            this.shape_637.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_637.setTransform(-617.7784,-33.0972,0.7277,0.3104,-90);

            this.shape_638 = new cjs.Shape();
            this.shape_638.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_638.setTransform(-617.7027,-95.1784,0.7277,0.3104);

            this.shape_639 = new cjs.Shape();
            this.shape_639.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_639.setTransform(-617.7784,-94.6472,0.7277,0.3104,-90);

            this.shape_640 = new cjs.Shape();
            this.shape_640.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_640.setTransform(-617.7027,-154.9284,0.7277,0.3104);

            this.shape_641 = new cjs.Shape();
            this.shape_641.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_641.setTransform(-617.7784,-154.5972,0.7277,0.3104,-90);

            this.shape_642 = new cjs.Shape();
            this.shape_642.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_642.setTransform(-617.7027,-216.7284,0.7277,0.3104);

            this.shape_643 = new cjs.Shape();
            this.shape_643.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_643.setTransform(-617.7784,-216.0472,0.7277,0.3104,-90);

            this.shape_644 = new cjs.Shape();
            this.shape_644.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_644.setTransform(-617.7027,-277.0284,0.7277,0.3104);

            this.shape_645 = new cjs.Shape();
            this.shape_645.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_645.setTransform(-617.7784,-276.3472,0.7277,0.3104,-90);

            this.shape_646 = new cjs.Shape();
            this.shape_646.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_646.setTransform(-617.7027,-338.3784,0.7277,0.3104);

            this.shape_647 = new cjs.Shape();
            this.shape_647.graphics.f("#FFFFFF").s().p("AgTDIIAAmPIAnAAIAAGPg");
            this.shape_647.setTransform(-617.7784,-338.1972,0.7277,0.3104,-90);

            this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_647},{t:this.shape_646},{t:this.shape_645},{t:this.shape_644},{t:this.shape_643},{t:this.shape_642},{t:this.shape_641},{t:this.shape_640},{t:this.shape_639},{t:this.shape_638},{t:this.shape_637},{t:this.shape_636},{t:this.shape_635},{t:this.shape_634},{t:this.shape_633},{t:this.shape_632},{t:this.shape_631},{t:this.shape_630},{t:this.shape_629},{t:this.shape_628},{t:this.shape_627},{t:this.shape_626},{t:this.shape_625},{t:this.shape_624},{t:this.shape_623},{t:this.shape_622},{t:this.shape_621},{t:this.shape_620},{t:this.shape_619},{t:this.shape_618},{t:this.shape_617},{t:this.shape_616},{t:this.shape_615},{t:this.shape_614},{t:this.shape_613},{t:this.shape_612},{t:this.shape_611},{t:this.shape_610},{t:this.shape_609},{t:this.shape_608},{t:this.shape_607},{t:this.shape_606},{t:this.shape_605},{t:this.shape_604},{t:this.shape_603},{t:this.shape_602},{t:this.shape_601},{t:this.shape_600},{t:this.shape_599},{t:this.shape_598},{t:this.shape_597},{t:this.shape_596},{t:this.shape_595},{t:this.shape_594},{t:this.shape_593},{t:this.shape_592},{t:this.shape_591},{t:this.shape_590},{t:this.shape_589},{t:this.shape_588},{t:this.shape_587},{t:this.shape_586},{t:this.shape_585},{t:this.shape_584},{t:this.shape_583},{t:this.shape_582},{t:this.shape_581},{t:this.shape_580},{t:this.shape_579},{t:this.shape_578},{t:this.shape_577},{t:this.shape_576},{t:this.shape_575},{t:this.shape_574},{t:this.shape_573},{t:this.shape_572},{t:this.shape_571},{t:this.shape_570},{t:this.shape_569},{t:this.shape_568},{t:this.shape_567},{t:this.shape_566},{t:this.shape_565},{t:this.shape_564},{t:this.shape_563},{t:this.shape_562},{t:this.shape_561},{t:this.shape_560},{t:this.shape_559},{t:this.shape_558},{t:this.shape_557},{t:this.shape_556},{t:this.shape_555},{t:this.shape_554},{t:this.shape_553},{t:this.shape_552},{t:this.shape_551},{t:this.shape_550},{t:this.shape_549},{t:this.shape_548},{t:this.shape_547},{t:this.shape_546},{t:this.shape_545},{t:this.shape_544},{t:this.shape_543},{t:this.shape_542},{t:this.shape_541},{t:this.shape_540},{t:this.shape_539},{t:this.shape_538},{t:this.shape_537},{t:this.shape_536},{t:this.shape_535},{t:this.shape_534},{t:this.shape_533},{t:this.shape_532},{t:this.shape_531},{t:this.shape_530},{t:this.shape_529},{t:this.shape_528},{t:this.shape_527},{t:this.shape_526},{t:this.shape_525},{t:this.shape_524},{t:this.shape_523},{t:this.shape_522},{t:this.shape_521},{t:this.shape_520},{t:this.shape_519},{t:this.shape_518},{t:this.shape_517},{t:this.shape_516},{t:this.shape_515},{t:this.shape_514},{t:this.shape_513},{t:this.shape_512},{t:this.shape_511},{t:this.shape_510},{t:this.shape_509},{t:this.shape_508},{t:this.shape_507},{t:this.shape_506},{t:this.shape_505},{t:this.shape_504},{t:this.shape_503},{t:this.shape_502},{t:this.shape_501},{t:this.shape_500},{t:this.shape_499},{t:this.shape_498},{t:this.shape_497},{t:this.shape_496},{t:this.shape_495},{t:this.shape_494},{t:this.shape_493},{t:this.shape_492},{t:this.shape_491},{t:this.shape_490},{t:this.shape_489},{t:this.shape_488},{t:this.shape_487},{t:this.shape_486},{t:this.shape_485},{t:this.shape_484},{t:this.shape_483},{t:this.shape_482},{t:this.shape_481},{t:this.shape_480},{t:this.shape_479},{t:this.shape_478},{t:this.shape_477},{t:this.shape_476},{t:this.shape_475},{t:this.shape_474},{t:this.shape_473},{t:this.shape_472},{t:this.shape_471},{t:this.shape_470},{t:this.shape_469},{t:this.shape_468},{t:this.shape_467},{t:this.shape_466},{t:this.shape_465},{t:this.shape_464},{t:this.shape_463},{t:this.shape_462},{t:this.shape_461},{t:this.shape_460},{t:this.shape_459},{t:this.shape_458},{t:this.shape_457},{t:this.shape_456},{t:this.shape_455},{t:this.shape_454},{t:this.shape_453},{t:this.shape_452},{t:this.shape_451},{t:this.shape_450},{t:this.shape_449},{t:this.shape_448},{t:this.shape_447},{t:this.shape_446},{t:this.shape_445},{t:this.shape_444},{t:this.shape_443},{t:this.shape_442},{t:this.shape_441},{t:this.shape_440},{t:this.shape_439},{t:this.shape_438},{t:this.shape_437},{t:this.shape_436},{t:this.shape_435},{t:this.shape_434},{t:this.shape_433},{t:this.shape_432},{t:this.shape_431},{t:this.shape_430},{t:this.shape_429},{t:this.shape_428},{t:this.shape_427},{t:this.shape_426},{t:this.shape_425},{t:this.shape_424},{t:this.shape_423},{t:this.shape_422},{t:this.shape_421},{t:this.shape_420},{t:this.shape_419},{t:this.shape_418},{t:this.shape_417},{t:this.shape_416},{t:this.shape_415},{t:this.shape_414},{t:this.shape_413},{t:this.shape_412},{t:this.shape_411},{t:this.shape_410},{t:this.shape_409},{t:this.shape_408},{t:this.shape_407},{t:this.shape_406},{t:this.shape_405},{t:this.shape_404},{t:this.shape_403},{t:this.shape_402},{t:this.shape_401},{t:this.shape_400},{t:this.shape_399},{t:this.shape_398},{t:this.shape_397},{t:this.shape_396},{t:this.shape_395},{t:this.shape_394},{t:this.shape_393},{t:this.shape_392},{t:this.shape_391},{t:this.shape_390},{t:this.shape_389},{t:this.shape_388},{t:this.shape_387},{t:this.shape_386},{t:this.shape_385},{t:this.shape_384},{t:this.shape_383},{t:this.shape_382},{t:this.shape_381},{t:this.shape_380},{t:this.shape_379},{t:this.shape_378},{t:this.shape_377},{t:this.shape_376},{t:this.shape_375},{t:this.shape_374},{t:this.shape_373},{t:this.shape_372},{t:this.shape_371},{t:this.shape_370},{t:this.shape_369},{t:this.shape_368},{t:this.shape_367},{t:this.shape_366},{t:this.shape_365},{t:this.shape_364},{t:this.shape_363},{t:this.shape_362},{t:this.shape_361},{t:this.shape_360},{t:this.shape_359},{t:this.shape_358},{t:this.shape_357},{t:this.shape_356},{t:this.shape_355},{t:this.shape_354},{t:this.shape_353},{t:this.shape_352},{t:this.shape_351},{t:this.shape_350},{t:this.shape_349},{t:this.shape_348},{t:this.shape_347},{t:this.shape_346},{t:this.shape_345},{t:this.shape_344},{t:this.shape_343},{t:this.shape_342},{t:this.shape_341},{t:this.shape_340},{t:this.shape_339},{t:this.shape_338},{t:this.shape_337},{t:this.shape_336},{t:this.shape_335},{t:this.shape_334},{t:this.shape_333},{t:this.shape_332},{t:this.shape_331},{t:this.shape_330},{t:this.shape_329},{t:this.shape_328},{t:this.shape_327},{t:this.shape_326},{t:this.shape_325},{t:this.shape_324},{t:this.shape_323},{t:this.shape_322},{t:this.shape_321},{t:this.shape_320},{t:this.shape_319},{t:this.shape_318},{t:this.shape_317},{t:this.shape_316},{t:this.shape_315},{t:this.shape_314},{t:this.shape_313},{t:this.shape_312},{t:this.shape_311},{t:this.shape_310},{t:this.shape_309},{t:this.shape_308},{t:this.shape_307},{t:this.shape_306},{t:this.shape_305},{t:this.shape_304},{t:this.shape_303},{t:this.shape_302},{t:this.shape_301},{t:this.shape_300},{t:this.shape_299},{t:this.shape_298},{t:this.shape_297},{t:this.shape_296},{t:this.shape_295},{t:this.shape_294},{t:this.shape_293},{t:this.shape_292},{t:this.shape_291},{t:this.shape_290},{t:this.shape_289},{t:this.shape_288},{t:this.shape_287},{t:this.shape_286},{t:this.shape_285},{t:this.shape_284},{t:this.shape_283},{t:this.shape_282},{t:this.shape_281},{t:this.shape_280},{t:this.shape_279},{t:this.shape_278},{t:this.shape_277},{t:this.shape_276},{t:this.shape_275},{t:this.shape_274},{t:this.shape_273},{t:this.shape_272},{t:this.shape_271},{t:this.shape_270},{t:this.shape_269},{t:this.shape_268},{t:this.shape_267},{t:this.shape_266},{t:this.shape_265},{t:this.shape_264},{t:this.shape_263},{t:this.shape_262},{t:this.shape_261},{t:this.shape_260},{t:this.shape_259},{t:this.shape_258},{t:this.shape_257},{t:this.shape_256},{t:this.shape_255},{t:this.shape_254},{t:this.shape_253},{t:this.shape_252},{t:this.shape_251},{t:this.shape_250},{t:this.shape_249},{t:this.shape_248},{t:this.shape_247},{t:this.shape_246},{t:this.shape_245},{t:this.shape_244},{t:this.shape_243},{t:this.shape_242},{t:this.shape_241},{t:this.shape_240},{t:this.shape_239},{t:this.shape_238},{t:this.shape_237},{t:this.shape_236},{t:this.shape_235},{t:this.shape_234},{t:this.shape_233},{t:this.shape_232},{t:this.shape_231},{t:this.shape_230},{t:this.shape_229},{t:this.shape_228},{t:this.shape_227},{t:this.shape_226},{t:this.shape_225},{t:this.shape_224},{t:this.shape_223},{t:this.shape_222},{t:this.shape_221},{t:this.shape_220},{t:this.shape_219},{t:this.shape_218},{t:this.shape_217},{t:this.shape_216},{t:this.shape_215},{t:this.shape_214},{t:this.shape_213},{t:this.shape_212},{t:this.shape_211},{t:this.shape_210},{t:this.shape_209},{t:this.shape_208},{t:this.shape_207},{t:this.shape_206},{t:this.shape_205},{t:this.shape_204},{t:this.shape_203},{t:this.shape_202},{t:this.shape_201},{t:this.shape_200},{t:this.shape_199},{t:this.shape_198},{t:this.shape_197},{t:this.shape_196},{t:this.shape_195},{t:this.shape_194},{t:this.shape_193},{t:this.shape_192},{t:this.shape_191},{t:this.shape_190},{t:this.shape_189},{t:this.shape_188},{t:this.shape_187},{t:this.shape_186},{t:this.shape_185},{t:this.shape_184},{t:this.shape_183},{t:this.shape_182},{t:this.shape_181},{t:this.shape_180},{t:this.shape_179},{t:this.shape_178},{t:this.shape_177},{t:this.shape_176},{t:this.shape_175},{t:this.shape_174},{t:this.shape_173},{t:this.shape_172},{t:this.shape_171},{t:this.shape_170},{t:this.shape_169},{t:this.shape_168},{t:this.shape_167},{t:this.shape_166},{t:this.shape_165},{t:this.shape_164},{t:this.shape_163},{t:this.shape_162},{t:this.shape_161},{t:this.shape_160},{t:this.shape_159},{t:this.shape_158},{t:this.shape_157},{t:this.shape_156},{t:this.shape_155},{t:this.shape_154},{t:this.shape_153},{t:this.shape_152},{t:this.shape_151},{t:this.shape_150},{t:this.shape_149},{t:this.shape_148},{t:this.shape_147},{t:this.shape_146},{t:this.shape_145},{t:this.shape_144},{t:this.shape_143},{t:this.shape_142},{t:this.shape_141},{t:this.shape_140},{t:this.shape_139},{t:this.shape_138},{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

        }).prototype = p = new cjs.MovieClip();
        p.nominalBounds = new cjs.Rectangle(-624,-344.6,1248,689.2);


        // stage content:
        (lib.CMA_Cross_S02 = function(mode,startPosition,loop) {
            this.initialize(mode,startPosition,loop,{});

            // color copy 5
            this.shape = new cjs.Shape();
            this.shape.graphics.f("#333333").s().p("Egx/AcIMAAAg4PMBj/AAAMAAAA4Pg");
            this.shape.setTransform(1600,179.975);
            this.shape._off = true;

            this.timeline.addTween(cjs.Tween.get(this.shape).wait(85).to({_off:false},0).wait(1).to({x:1536},0).wait(1).to({x:1472},0).wait(1).to({x:1408},0).wait(1).to({x:1344},0).wait(1).to({x:1280},0).wait(1).to({x:1216},0).wait(1).to({x:1152},0).wait(1).to({x:1088},0).wait(1).to({x:1024},0).wait(1).to({x:960},0).wait(1));

            // color copy 6
            this.shape_1 = new cjs.Shape();
            this.shape_1.graphics.f("#333333").s().p("Egx/AcIMAAAg4PMBj/AAAMAAAA4Pg");
            this.shape_1.setTransform(-320,540.025);
            this.shape_1._off = true;

            this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(85).to({_off:false},0).wait(1).to({x:-256},0).wait(1).to({x:-192},0).wait(1).to({x:-128},0).wait(1).to({x:-64},0).wait(1).to({x:0},0).wait(1).to({x:64},0).wait(1).to({x:128},0).wait(1).to({x:192},0).wait(1).to({x:256},0).wait(1).to({x:320},0).wait(1));

            // color copy 7
            this.shape_2 = new cjs.Shape();
            this.shape_2.graphics.f("#333333").s().p("Egx/AcIMAAAg4PMBj/AAAMAAAA4Pg");
            this.shape_2.setTransform(320.025,-179.975);
            this.shape_2._off = true;

            this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(85).to({_off:false},0).wait(1).to({y:-143.98},0).wait(1).to({y:-107.985},0).wait(1).to({y:-71.99},0).wait(1).to({y:-35.995},0).wait(1).to({y:0},0).wait(1).to({y:35.995},0).wait(1).to({y:71.99},0).wait(1).to({y:107.985},0).wait(1).to({y:143.98},0).wait(1).to({y:179.975},0).wait(1));

            // color copy 8
            this.shape_3 = new cjs.Shape();
            this.shape_3.graphics.f("#333333").s().p("Egx/AcIMAAAg4PMBj/AAAMAAAA4Pg");
            this.shape_3.setTransform(959.975,899.975);
            this.shape_3._off = true;

            this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(85).to({_off:false},0).wait(1).to({y:863.98},0).wait(1).to({y:827.985},0).wait(1).to({y:791.99},0).wait(1).to({y:755.995},0).wait(1).to({y:720},0).wait(1).to({y:684.005},0).wait(1).to({y:648.01},0).wait(1).to({y:612.015},0).wait(1).to({y:576.02},0).wait(1).to({y:540.025},0).wait(1));

            // lines
            this.shape_4 = new cjs.Shape();
            this.shape_4.graphics.f("#339999").s().p("AhPAOIAAgbICfAAIAAAbg");
            this.shape_4.setTransform(92.4026,50.5184,0.3448,0.3448);

            this.shape_5 = new cjs.Shape();
            this.shape_5.graphics.f("#339999").s().p("AhPAoIAAhPICfAAIAABPg");
            this.shape_5.setTransform(92.4026,51.415,0.3448,0.3448);

            this.shape_6 = new cjs.Shape();
            this.shape_6.graphics.f("#339999").s().p("AhPBKIAAiTICfAAIAACTg");
            this.shape_6.setTransform(92.4026,52.5873,0.3448,0.3448);

            this.shape_7 = new cjs.Shape();
            this.shape_7.graphics.f("#339999").s().p("AhPCGIAAkLICfAAIAAELg");
            this.shape_7.setTransform(92.4026,54.6563,0.3448,0.3448);

            this.shape_8 = new cjs.Shape();
            this.shape_8.graphics.f("#339999").s().p("AhPDiIAAnDICfAAIAAHDg");
            this.shape_8.setTransform(92.4026,57.8286,0.3448,0.3448);

            this.shape_9 = new cjs.Shape();
            this.shape_9.graphics.f("#339999").s().p("AhPFqIAArTICfAAIAALTg");
            this.shape_9.setTransform(92.4026,62.5181,0.3448,0.3448);

            this.shape_10 = new cjs.Shape();
            this.shape_10.graphics.f("#339999").s().p("AgbDBIAAmBIA2AAIAAGBg");
            this.shape_10.setTransform(1176.65,591.5);

            this.shape_11 = new cjs.Shape();
            this.shape_11.graphics.f("#339999").s().p("AgbD+IAAn7IA2AAIAAH7g");
            this.shape_11.setTransform(1176.65,597.625);

            this.shape_12 = new cjs.Shape();
            this.shape_12.graphics.f("#339999").s().p("AgbEuIAApbIA2AAIAAJbg");
            this.shape_12.setTransform(1176.65,602.4);

            this.shape_13 = new cjs.Shape();
            this.shape_13.graphics.f("#339999").s().p("AgbFMIAAqXIA2AAIAAKXg");
            this.shape_13.setTransform(1176.65,605.35);

            this.shape_14 = new cjs.Shape();
            this.shape_14.graphics.f("#339999").s().p("AgbFnIAArNIA2AAIAALNg");
            this.shape_14.setTransform(1176.65,608.05);

            this.shape_15 = new cjs.Shape();
            this.shape_15.graphics.f("#339999").s().p("AgbF3IAArtIA2AAIAALtg");
            this.shape_15.setTransform(1176.65,609.7);

            this.shape_16 = new cjs.Shape();
            this.shape_16.graphics.f("#339999").s().p("AgbGEIAAsHIA2AAIAAMHg");
            this.shape_16.setTransform(1176.65,611.025);

            this.shape_17 = new cjs.Shape();
            this.shape_17.graphics.f("#339999").s().p("AgbGOIAAsbIA2AAIAAMbg");
            this.shape_17.setTransform(1176.65,611.975);

            this.shape_18 = new cjs.Shape();
            this.shape_18.graphics.f("#339999").s().p("AgbGWIAAsrIA2AAIAAMrg");
            this.shape_18.setTransform(1176.65,612.8);

            this.shape_19 = new cjs.Shape();
            this.shape_19.graphics.f("#339999").s().p("AgbGaIAAs0IA2AAIAAM0g");
            this.shape_19.setTransform(1176.65,613.25);

            this.shape_20 = new cjs.Shape();
            this.shape_20.graphics.f("#339999").s().p("AgbGWIAAssIA2AAIAAMsg");
            this.shape_20.setTransform(1176.65,613.65);

            this.shape_21 = new cjs.Shape();
            this.shape_21.graphics.f("#339999").s().p("AgbGNIAAsaIA2AAIAAMag");
            this.shape_21.setTransform(1176.65,614.55);

            this.shape_22 = new cjs.Shape();
            this.shape_22.graphics.f("#339999").s().p("AgbGAIAAr/IA2AAIAAL/g");
            this.shape_22.setTransform(1176.65,615.85);

            this.shape_23 = new cjs.Shape();
            this.shape_23.graphics.f("#339999").s().p("AgbFtIAArZIA2AAIAALZg");
            this.shape_23.setTransform(1176.65,617.8);

            this.shape_24 = new cjs.Shape();
            this.shape_24.graphics.f("#339999").s().p("AgbFNIAAqZIA2AAIAAKZg");
            this.shape_24.setTransform(1176.65,621.025);

            this.shape_25 = new cjs.Shape();
            this.shape_25.graphics.f("#339999").s().p("AgbEeIAAo7IA2AAIAAI7g");
            this.shape_25.setTransform(1176.65,625.65);

            this.shape_26 = new cjs.Shape();
            this.shape_26.graphics.f("#339999").s().p("AgbDZIAAmxIA2AAIAAGxg");
            this.shape_26.setTransform(1176.65,632.625);

            this.shape_27 = new cjs.Shape();
            this.shape_27.graphics.f("#339999").s().p("AgbCcIAAk3IA2AAIAAE3g");
            this.shape_27.setTransform(1176.65,638.675);

            this.shape_28 = new cjs.Shape();
            this.shape_28.graphics.f("#339999").s().p("AgbBrIAAjVIA2AAIAADVg");
            this.shape_28.setTransform(1176.65,643.575);

            this.shape_29 = new cjs.Shape();
            this.shape_29.graphics.f("#339999").s().p("AgbBOIAAibIA2AAIAACbg");
            this.shape_29.setTransform(1176.65,646.475);

            this.shape_30 = new cjs.Shape();
            this.shape_30.graphics.f("#339999").s().p("AgbA0IAAhnIA2AAIAABng");
            this.shape_30.setTransform(1176.65,649.125);

            this.shape_31 = new cjs.Shape();
            this.shape_31.graphics.f("#339999").s().p("AgbAjIAAhFIA2AAIAABFg");
            this.shape_31.setTransform(1176.65,650.825);

            this.shape_32 = new cjs.Shape();
            this.shape_32.graphics.f("#339999").s().p("AgbAXIAAgtIA2AAIAAAtg");
            this.shape_32.setTransform(1176.65,652);

            this.shape_33 = new cjs.Shape();
            this.shape_33.graphics.f("#339999").s().p("AgbANIAAgaIA2AAIAAAag");
            this.shape_33.setTransform(1176.65,652.95);

            this.shape_34 = new cjs.Shape();
            this.shape_34.graphics.f("#339999").s().p("AgbAFIAAgKIA2AAIAAAKg");
            this.shape_34.setTransform(1176.65,653.75);

            this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_4}]},10).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[]},1).wait(55));

            // lines
            this.shape_35 = new cjs.Shape();
            this.shape_35.graphics.f("#339999").s().p("AgNBQIAAifIAbAAIAACfg");
            this.shape_35.setTransform(133.6342,89.9915,0.3448,0.3448);

            this.shape_36 = new cjs.Shape();
            this.shape_36.graphics.f("#339999").s().p("AgnBQIAAifIBPAAIAACfg");
            this.shape_36.setTransform(132.7377,89.9915,0.3448,0.3448);

            this.shape_37 = new cjs.Shape();
            this.shape_37.graphics.f("#339999").s().p("AhJBQIAAifICTAAIAACfg");
            this.shape_37.setTransform(131.5653,89.9915,0.3448,0.3448);

            this.shape_38 = new cjs.Shape();
            this.shape_38.graphics.f("#339999").s().p("AiFBQIAAifIELAAIAACfg");
            this.shape_38.setTransform(129.4964,89.9915,0.3448,0.3448);

            this.shape_39 = new cjs.Shape();
            this.shape_39.graphics.f("#339999").s().p("AjhBQIAAifIHDAAIAACfg");
            this.shape_39.setTransform(126.3241,89.9915,0.3448,0.3448);

            this.shape_40 = new cjs.Shape();
            this.shape_40.graphics.f("#339999").s().p("AlpBQIAAifILTAAIAACfg");
            this.shape_40.setTransform(121.6346,89.9915,0.3448,0.3448);

            this.shape_41 = new cjs.Shape();
            this.shape_41.graphics.f("#339999").s().p("AjAAcIAAg3IGBAAIAAA3g");
            this.shape_41.setTransform(1199.05,612.15);

            this.shape_42 = new cjs.Shape();
            this.shape_42.graphics.f("#339999").s().p("Aj9AcIAAg3IH7AAIAAA3g");
            this.shape_42.setTransform(1192.925,612.15);

            this.shape_43 = new cjs.Shape();
            this.shape_43.graphics.f("#339999").s().p("AktAcIAAg3IJbAAIAAA3g");
            this.shape_43.setTransform(1188.15,612.15);

            this.shape_44 = new cjs.Shape();
            this.shape_44.graphics.f("#339999").s().p("AlLAcIAAg3IKWAAIAAA3g");
            this.shape_44.setTransform(1185.2,612.15);

            this.shape_45 = new cjs.Shape();
            this.shape_45.graphics.f("#339999").s().p("AlmAcIAAg3ILMAAIAAA3g");
            this.shape_45.setTransform(1182.5,612.15);

            this.shape_46 = new cjs.Shape();
            this.shape_46.graphics.f("#339999").s().p("Al2AcIAAg3ILtAAIAAA3g");
            this.shape_46.setTransform(1180.85,612.15);

            this.shape_47 = new cjs.Shape();
            this.shape_47.graphics.f("#339999").s().p("AmDAcIAAg3IMHAAIAAA3g");
            this.shape_47.setTransform(1179.525,612.15);

            this.shape_48 = new cjs.Shape();
            this.shape_48.graphics.f("#339999").s().p("AmNAcIAAg3IMbAAIAAA3g");
            this.shape_48.setTransform(1178.575,612.15);

            this.shape_49 = new cjs.Shape();
            this.shape_49.graphics.f("#339999").s().p("AmVAcIAAg3IMrAAIAAA3g");
            this.shape_49.setTransform(1177.75,612.15);

            this.shape_50 = new cjs.Shape();
            this.shape_50.graphics.f("#339999").s().p("AmZAcIAAg3IMzAAIAAA3g");
            this.shape_50.setTransform(1177.3,612.15);

            this.shape_51 = new cjs.Shape();
            this.shape_51.graphics.f("#339999").s().p("AmMAcIAAg3IMZAAIAAA3g");
            this.shape_51.setTransform(1176,612.15);

            this.shape_52 = new cjs.Shape();
            this.shape_52.graphics.f("#339999").s().p("Al/AcIAAg3IL/AAIAAA3g");
            this.shape_52.setTransform(1174.7,612.15);

            this.shape_53 = new cjs.Shape();
            this.shape_53.graphics.f("#339999").s().p("AlsAcIAAg3ILZAAIAAA3g");
            this.shape_53.setTransform(1172.75,612.15);

            this.shape_54 = new cjs.Shape();
            this.shape_54.graphics.f("#339999").s().p("AlMAcIAAg3IKZAAIAAA3g");
            this.shape_54.setTransform(1169.525,612.15);

            this.shape_55 = new cjs.Shape();
            this.shape_55.graphics.f("#339999").s().p("AkdAcIAAg3II7AAIAAA3g");
            this.shape_55.setTransform(1164.9,612.15);

            this.shape_56 = new cjs.Shape();
            this.shape_56.graphics.f("#339999").s().p("AjYAcIAAg3IGxAAIAAA3g");
            this.shape_56.setTransform(1157.925,612.15);

            this.shape_57 = new cjs.Shape();
            this.shape_57.graphics.f("#339999").s().p("AibAcIAAg3IE3AAIAAA3g");
            this.shape_57.setTransform(1151.875,612.15);

            this.shape_58 = new cjs.Shape();
            this.shape_58.graphics.f("#339999").s().p("AhqAcIAAg3IDVAAIAAA3g");
            this.shape_58.setTransform(1146.975,612.15);

            this.shape_59 = new cjs.Shape();
            this.shape_59.graphics.f("#339999").s().p("AhNAcIAAg3ICbAAIAAA3g");
            this.shape_59.setTransform(1144.075,612.15);

            this.shape_60 = new cjs.Shape();
            this.shape_60.graphics.f("#339999").s().p("AgzAcIAAg3IBnAAIAAA3g");
            this.shape_60.setTransform(1141.425,612.15);

            this.shape_61 = new cjs.Shape();
            this.shape_61.graphics.f("#339999").s().p("AgiAcIAAg3IBFAAIAAA3g");
            this.shape_61.setTransform(1139.725,612.15);

            this.shape_62 = new cjs.Shape();
            this.shape_62.graphics.f("#339999").s().p("AgWAcIAAg3IAtAAIAAA3g");
            this.shape_62.setTransform(1138.55,612.15);

            this.shape_63 = new cjs.Shape();
            this.shape_63.graphics.f("#339999").s().p("AgMAcIAAg3IAaAAIAAA3g");
            this.shape_63.setTransform(1137.6,612.15);

            this.shape_64 = new cjs.Shape();
            this.shape_64.graphics.f("#339999").s().p("AgEAcIAAg3IAJAAIAAA3g");
            this.shape_64.setTransform(1136.8,612.15);

            this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_35}]},10).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49,p:{x:1177.75}}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_49,p:{x:1176.9}}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[]},1).wait(55));

            // lines
            this.shape_65 = new cjs.Shape();
            this.shape_65.graphics.f("#339999").s().p("AhPAOIAAgbICfAAIAAAbg");
            this.shape_65.setTransform(92.4026,50.5184,0.3448,0.3448);

            this.shape_66 = new cjs.Shape();
            this.shape_66.graphics.f("#339999").s().p("AhPAoIAAhPICfAAIAABPg");
            this.shape_66.setTransform(92.4026,51.415,0.3448,0.3448);

            this.shape_67 = new cjs.Shape();
            this.shape_67.graphics.f("#339999").s().p("AhPBKIAAiTICfAAIAACTg");
            this.shape_67.setTransform(92.4026,52.5873,0.3448,0.3448);

            this.shape_68 = new cjs.Shape();
            this.shape_68.graphics.f("#339999").s().p("AhPCGIAAkLICfAAIAAELg");
            this.shape_68.setTransform(92.4026,54.6563,0.3448,0.3448);

            this.shape_69 = new cjs.Shape();
            this.shape_69.graphics.f("#339999").s().p("AhPDiIAAnDICfAAIAAHDg");
            this.shape_69.setTransform(92.4026,57.8286,0.3448,0.3448);

            this.shape_70 = new cjs.Shape();
            this.shape_70.graphics.f("#339999").s().p("AhPFqIAArTICfAAIAALTg");
            this.shape_70.setTransform(92.4026,62.5181,0.3448,0.3448);

            this.shape_71 = new cjs.Shape();
            this.shape_71.graphics.f("#339999").s().p("AhPIwIAAxfICfAAIAARfg");
            this.shape_71.setTransform(92.4026,69.3455,0.3448,0.3448);

            this.shape_72 = new cjs.Shape();
            this.shape_72.graphics.f("#339999").s().p("AhPLiIAA3DICfAAIAAXDg");
            this.shape_72.setTransform(92.4026,75.4833,0.3448,0.3448);

            this.shape_73 = new cjs.Shape();
            this.shape_73.graphics.f("#339999").s().p("AhPNsIAA7XICfAAIAAbXg");
            this.shape_73.setTransform(92.4026,80.2418,0.3448,0.3448);

            this.shape_74 = new cjs.Shape();
            this.shape_74.graphics.f("#339999").s().p("AhPPCIAA+DICfAAIAAeDg");
            this.shape_74.setTransform(92.4026,83.2072,0.3448,0.3448);

            this.shape_75 = new cjs.Shape();
            this.shape_75.graphics.f("#339999").s().p("AhPQQMAAAggfICfAAMAAAAgfg");
            this.shape_75.setTransform(92.4026,85.8968,0.3448,0.3448);

            this.shape_76 = new cjs.Shape();
            this.shape_76.graphics.f("#339999").s().p("AhPRAMAAAgh/ICfAAMAAAAh/g");
            this.shape_76.setTransform(92.4026,87.5519,0.3448,0.3448);

            this.shape_77 = new cjs.Shape();
            this.shape_77.graphics.f("#339999").s().p("AhPRmMAAAgjLICfAAMAAAAjLg");
            this.shape_77.setTransform(92.4026,88.8622,0.3448,0.3448);

            this.shape_78 = new cjs.Shape();
            this.shape_78.graphics.f("#339999").s().p("AhPSCMAAAgkDICfAAMAAAAkDg");
            this.shape_78.setTransform(92.4026,89.8277,0.3448,0.3448);

            this.shape_79 = new cjs.Shape();
            this.shape_79.graphics.f("#339999").s().p("AhPSaMAAAgkzICfAAMAAAAkzg");
            this.shape_79.setTransform(92.4026,90.6553,0.3448,0.3448);

            this.shape_80 = new cjs.Shape();
            this.shape_80.graphics.f("#339999").s().p("AhPSmMAAAglLICfAAMAAAAlLg");
            this.shape_80.setTransform(92.4026,91.0949,0.3448,0.3448);

            this.shape_81 = new cjs.Shape();
            this.shape_81.graphics.f("#339999").s().p("AhPSbMAAAgk1ICfAAMAAAAk1g");
            this.shape_81.setTransform(92.4026,91.5001,0.3448,0.3448);

            this.shape_82 = new cjs.Shape();
            this.shape_82.graphics.f("#339999").s().p("AhPSBMAAAgkBICfAAMAAAAkBg");
            this.shape_82.setTransform(92.4026,92.3966,0.3448,0.3448);

            this.shape_83 = new cjs.Shape();
            this.shape_83.graphics.f("#339999").s().p("AhPRbMAAAgi1ICfAAMAAAAi1g");
            this.shape_83.setTransform(92.4026,93.7069,0.3448,0.3448);

            this.shape_84 = new cjs.Shape();
            this.shape_84.graphics.f("#339999").s().p("AhPQjMAAAghFICfAAMAAAAhFg");
            this.shape_84.setTransform(92.4026,95.6379,0.3448,0.3448);

            this.shape_85 = new cjs.Shape();
            this.shape_85.graphics.f("#339999").s().p("AhPPFIAA+JICfAAIAAeJg");
            this.shape_85.setTransform(92.4026,98.8792,0.3448,0.3448);

            this.shape_86 = new cjs.Shape();
            this.shape_86.graphics.f("#339999").s().p("AhPM/IAA59ICfAAIAAZ9g");
            this.shape_86.setTransform(92.4026,103.4997,0.3448,0.3448);

            this.shape_87 = new cjs.Shape();
            this.shape_87.graphics.f("#339999").s().p("AhPJ1IAAzpICfAAIAATpg");
            this.shape_87.setTransform(92.4026,110.4651,0.3448,0.3448);

            this.shape_88 = new cjs.Shape();
            this.shape_88.graphics.f("#339999").s().p("AhPHFIAAuJICfAAIAAOJg");
            this.shape_88.setTransform(92.4026,116.5339,0.3448,0.3448);

            this.shape_89 = new cjs.Shape();
            this.shape_89.graphics.f("#339999").s().p("AhPE3IAAptICfAAIAAJtg");
            this.shape_89.setTransform(92.4026,121.4303,0.3448,0.3448);

            this.shape_90 = new cjs.Shape();
            this.shape_90.graphics.f("#339999").s().p("AhPDjIAAnFICfAAIAAHFg");
            this.shape_90.setTransform(92.4026,124.3268,0.3448,0.3448);

            this.shape_91 = new cjs.Shape();
            this.shape_91.graphics.f("#339999").s().p("AhPCWIAAkrICfAAIAAErg");
            this.shape_91.setTransform(92.4026,126.9819,0.3448,0.3448);

            this.shape_92 = new cjs.Shape();
            this.shape_92.graphics.f("#339999").s().p("AhPBlIAAjJICfAAIAADJg");
            this.shape_92.setTransform(92.4026,128.6715,0.3448,0.3448);

            this.shape_93 = new cjs.Shape();
            this.shape_93.graphics.f("#339999").s().p("AhPBDIAAiFICfAAIAACFg");
            this.shape_93.setTransform(92.4026,129.8438,0.3448,0.3448);

            this.shape_94 = new cjs.Shape();
            this.shape_94.graphics.f("#339999").s().p("AhPAnIAAhNICfAAIAABNg");
            this.shape_94.setTransform(92.4026,130.8093,0.3448,0.3448);

            this.shape_95 = new cjs.Shape();
            this.shape_95.graphics.f("#339999").s().p("AhPAQIAAgfICfAAIAAAfg");
            this.shape_95.setTransform(92.4026,131.6024,0.3448,0.3448);

            this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_65}]}).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_90}]},1).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_95}]},1).to({state:[]},1).wait(65));

            // lines
            this.shape_96 = new cjs.Shape();
            this.shape_96.graphics.f("#339999").s().p("AgNBQIAAifIAbAAIAACfg");
            this.shape_96.setTransform(133.6342,89.9915,0.3448,0.3448);

            this.shape_97 = new cjs.Shape();
            this.shape_97.graphics.f("#339999").s().p("AgnBQIAAifIBPAAIAACfg");
            this.shape_97.setTransform(132.7377,89.9915,0.3448,0.3448);

            this.shape_98 = new cjs.Shape();
            this.shape_98.graphics.f("#339999").s().p("AhJBQIAAifICTAAIAACfg");
            this.shape_98.setTransform(131.5653,89.9915,0.3448,0.3448);

            this.shape_99 = new cjs.Shape();
            this.shape_99.graphics.f("#339999").s().p("AiFBQIAAifIELAAIAACfg");
            this.shape_99.setTransform(129.4964,89.9915,0.3448,0.3448);

            this.shape_100 = new cjs.Shape();
            this.shape_100.graphics.f("#339999").s().p("AjhBQIAAifIHDAAIAACfg");
            this.shape_100.setTransform(126.3241,89.9915,0.3448,0.3448);

            this.shape_101 = new cjs.Shape();
            this.shape_101.graphics.f("#339999").s().p("AlpBQIAAifILTAAIAACfg");
            this.shape_101.setTransform(121.6346,89.9915,0.3448,0.3448);

            this.shape_102 = new cjs.Shape();
            this.shape_102.graphics.f("#339999").s().p("AovBQIAAifIRfAAIAACfg");
            this.shape_102.setTransform(114.8072,89.9915,0.3448,0.3448);

            this.shape_103 = new cjs.Shape();
            this.shape_103.graphics.f("#339999").s().p("ArhBQIAAifIXDAAIAACfg");
            this.shape_103.setTransform(108.6694,89.9915,0.3448,0.3448);

            this.shape_104 = new cjs.Shape();
            this.shape_104.graphics.f("#339999").s().p("AtrBQIAAifIbXAAIAACfg");
            this.shape_104.setTransform(103.9109,89.9915,0.3448,0.3448);

            this.shape_105 = new cjs.Shape();
            this.shape_105.graphics.f("#339999").s().p("AvBBQIAAifIeDAAIAACfg");
            this.shape_105.setTransform(100.9455,89.9915,0.3448,0.3448);

            this.shape_106 = new cjs.Shape();
            this.shape_106.graphics.f("#339999").s().p("AwPBQIAAifMAgfAAAIAACfg");
            this.shape_106.setTransform(98.2559,89.9915,0.3448,0.3448);

            this.shape_107 = new cjs.Shape();
            this.shape_107.graphics.f("#339999").s().p("Aw/BQIAAifMAh/AAAIAACfg");
            this.shape_107.setTransform(96.6008,89.9915,0.3448,0.3448);

            this.shape_108 = new cjs.Shape();
            this.shape_108.graphics.f("#339999").s().p("AxlBQIAAifMAjLAAAIAACfg");
            this.shape_108.setTransform(95.2905,89.9915,0.3448,0.3448);

            this.shape_109 = new cjs.Shape();
            this.shape_109.graphics.f("#339999").s().p("AyBBQIAAifMAkDAAAIAACfg");
            this.shape_109.setTransform(94.325,89.9915,0.3448,0.3448);

            this.shape_110 = new cjs.Shape();
            this.shape_110.graphics.f("#339999").s().p("AyZBQIAAifMAkzAAAIAACfg");
            this.shape_110.setTransform(93.4974,89.9915,0.3448,0.3448);

            this.shape_111 = new cjs.Shape();
            this.shape_111.graphics.f("#339999").s().p("AymBQIAAifMAlNAAAIAACfg");
            this.shape_111.setTransform(93.0578,89.9915,0.3448,0.3448);

            this.shape_112 = new cjs.Shape();
            this.shape_112.graphics.f("#339999").s().p("AyaBQIAAifMAk1AAAIAACfg");
            this.shape_112.setTransform(92.6526,89.9915,0.3448,0.3448);

            this.shape_113 = new cjs.Shape();
            this.shape_113.graphics.f("#339999").s().p("AyABQIAAifMAkBAAAIAACfg");
            this.shape_113.setTransform(91.7561,89.9915,0.3448,0.3448);

            this.shape_114 = new cjs.Shape();
            this.shape_114.graphics.f("#339999").s().p("AxaBQIAAifMAi1AAAIAACfg");
            this.shape_114.setTransform(90.4458,89.9915,0.3448,0.3448);

            this.shape_115 = new cjs.Shape();
            this.shape_115.graphics.f("#339999").s().p("AwiBQIAAifMAhFAAAIAACfg");
            this.shape_115.setTransform(88.5148,89.9915,0.3448,0.3448);

            this.shape_116 = new cjs.Shape();
            this.shape_116.graphics.f("#339999").s().p("AvEBQIAAifIeJAAIAACfg");
            this.shape_116.setTransform(85.2735,89.9915,0.3448,0.3448);

            this.shape_117 = new cjs.Shape();
            this.shape_117.graphics.f("#339999").s().p("As+BQIAAifIZ9AAIAACfg");
            this.shape_117.setTransform(80.6529,89.9915,0.3448,0.3448);

            this.shape_118 = new cjs.Shape();
            this.shape_118.graphics.f("#339999").s().p("Ap0BQIAAifITpAAIAACfg");
            this.shape_118.setTransform(73.6876,89.9915,0.3448,0.3448);

            this.shape_119 = new cjs.Shape();
            this.shape_119.graphics.f("#339999").s().p("AnEBQIAAifIOJAAIAACfg");
            this.shape_119.setTransform(67.6188,89.9915,0.3448,0.3448);

            this.shape_120 = new cjs.Shape();
            this.shape_120.graphics.f("#339999").s().p("Ak2BQIAAifIJtAAIAACfg");
            this.shape_120.setTransform(62.7224,89.9915,0.3448,0.3448);

            this.shape_121 = new cjs.Shape();
            this.shape_121.graphics.f("#339999").s().p("AjiBQIAAifIHFAAIAACfg");
            this.shape_121.setTransform(59.8259,89.9915,0.3448,0.3448);

            this.shape_122 = new cjs.Shape();
            this.shape_122.graphics.f("#339999").s().p("AiVBQIAAifIErAAIAACfg");
            this.shape_122.setTransform(57.1708,89.9915,0.3448,0.3448);

            this.shape_123 = new cjs.Shape();
            this.shape_123.graphics.f("#339999").s().p("AhkBQIAAifIDJAAIAACfg");
            this.shape_123.setTransform(55.4812,89.9915,0.3448,0.3448);

            this.shape_124 = new cjs.Shape();
            this.shape_124.graphics.f("#339999").s().p("AhCBQIAAifICFAAIAACfg");
            this.shape_124.setTransform(54.3088,89.9915,0.3448,0.3448);

            this.shape_125 = new cjs.Shape();
            this.shape_125.graphics.f("#339999").s().p("AgmBQIAAifIBNAAIAACfg");
            this.shape_125.setTransform(53.3433,89.9915,0.3448,0.3448);

            this.shape_126 = new cjs.Shape();
            this.shape_126.graphics.f("#339999").s().p("AgPBQIAAifIAfAAIAACfg");
            this.shape_126.setTransform(52.5502,89.9915,0.3448,0.3448);

            this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_96}]}).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_98}]},1).to({state:[{t:this.shape_99}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_101}]},1).to({state:[{t:this.shape_102}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_108}]},1).to({state:[{t:this.shape_109}]},1).to({state:[{t:this.shape_110}]},1).to({state:[{t:this.shape_111}]},1).to({state:[{t:this.shape_112}]},1).to({state:[{t:this.shape_113}]},1).to({state:[{t:this.shape_114}]},1).to({state:[{t:this.shape_115}]},1).to({state:[{t:this.shape_116}]},1).to({state:[{t:this.shape_117}]},1).to({state:[{t:this.shape_118}]},1).to({state:[{t:this.shape_119}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_121}]},1).to({state:[{t:this.shape_122}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_124}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_126}]},1).to({state:[]},1).wait(65));

            // color copy 4
            this.shape_127 = new cjs.Shape();
            this.shape_127.graphics.f("#333333").s().p("Egx/AcIMAAAg4PMBj/AAAMAAAA4Pg");
            this.shape_127.setTransform(960,179.975);

            this.timeline.addTween(cjs.Tween.get(this.shape_127).wait(1).to({x:1024},0).wait(1).to({x:1088},0).wait(1).to({x:1152},0).wait(1).to({x:1216},0).wait(1).to({x:1280},0).wait(1).to({x:1344},0).wait(1).to({x:1408},0).wait(1).to({x:1472},0).wait(1).to({x:1536},0).wait(1).to({x:1600},0).to({_off:true},1).wait(85));

            // color copy 3
            this.shape_128 = new cjs.Shape();
            this.shape_128.graphics.f("#333333").s().p("Egx/AcIMAAAg4PMBj/AAAMAAAA4Pg");
            this.shape_128.setTransform(320,540.025);

            this.timeline.addTween(cjs.Tween.get(this.shape_128).wait(1).to({x:256},0).wait(1).to({x:192},0).wait(1).to({x:128},0).wait(1).to({x:64},0).wait(1).to({x:0},0).wait(1).to({x:-64},0).wait(1).to({x:-128},0).wait(1).to({x:-192},0).wait(1).to({x:-256},0).wait(1).to({x:-320},0).to({_off:true},1).wait(85));

            // color copy 2
            this.shape_129 = new cjs.Shape();
            this.shape_129.graphics.f("#333333").s().p("Egx/AcIMAAAg4PMBj/AAAMAAAA4Pg");
            this.shape_129.setTransform(320.025,179.975);

            this.timeline.addTween(cjs.Tween.get(this.shape_129).wait(1).to({y:143.98},0).wait(1).to({y:107.985},0).wait(1).to({y:71.99},0).wait(1).to({y:35.995},0).wait(1).to({y:0},0).wait(1).to({y:-35.995},0).wait(1).to({y:-71.99},0).wait(1).to({y:-107.985},0).wait(1).to({y:-143.98},0).wait(1).to({y:-179.975},0).to({_off:true},1).wait(85));

            // color copy
            this.shape_130 = new cjs.Shape();
            this.shape_130.graphics.f("#333333").s().p("Egx/AcIMAAAg4PMBj/AAAMAAAA4Pg");
            this.shape_130.setTransform(959.975,540.025);

            this.timeline.addTween(cjs.Tween.get(this.shape_130).wait(1).to({y:576.02},0).wait(1).to({y:612.015},0).wait(1).to({y:648.01},0).wait(1).to({y:684.005},0).wait(1).to({y:720},0).wait(1).to({y:755.995},0).wait(1).to({y:791.99},0).wait(1).to({y:827.985},0).wait(1).to({y:863.98},0).wait(1).to({y:899.975},0).to({_off:true},1).wait(85));

            // text
            this.textbox_ = new cjs.Text("So what could it be worth\nin today’s market?\nLet’s look at the factors\nThat will influence your price…", "35px 'Poppins Medium'", "#FFFFFF");
            this.textbox_.name = "textbox_";
            this.textbox_.textAlign = "center";
            this.textbox_.lineHeight = 64;
            this.textbox_.lineWidth = 1022;
            this.textbox_.parent = this;
            this.textbox_.setTransform(644.6,253.7);
            this.textbox_._off = true;

            this.timeline.addTween(cjs.Tween.get(this.textbox_).wait(4).to({_off:false},0).wait(92));

            // Cover
            this.shape_131 = new cjs.Shape();
            this.shape_131.graphics.f("#333333").s().p("Eg/6AcLMAAAg4VMB/1AAAMAAAA4Vg");
            this.shape_131.setTransform(640.025,360);

            this.timeline.addTween(cjs.Tween.get(this.shape_131).wait(96));

            // Cross Element
            this.instance = new lib.Tween1("synched",0);
            this.instance.parent = this;
            this.instance.setTransform(638.5,357.1);
            this.instance.alpha = 0;
            this.instance._off = true;

            this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off:false},0).to({alpha:1},5).to({alpha:0},5).to({_off:true},1).wait(74));

            // Background
            this.instance_1 = new lib.MBBC("synched",0);
            this.instance_1.parent = this;
            this.instance_1.setTransform(640,360,1,1,0,0,0,640,360);

            this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(96));

            // Frames
            this.instance_2 = new lib.Frames_96("synched",0);
            this.instance_2.parent = this;
            this.instance_2.setTransform(640,360,1,1,0,0,0,640,360);

            this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(96));

        }).prototype = p = new cjs.MovieClip();
        p.nominalBounds = new cjs.Rectangle(0,0.1,1920,1079.9);
        // library properties:
        lib.properties = {
            id: '559B450ECF44B84B824008B0C5B4BC45',
            width: 1280,
            height: 720,
            fps: 24,
            color: "#FFFFFF",
            opacity: 1.00,
            manifest: [],
            preloads: []
        };



        // bootstrap callback support:

        (lib.Stage = function(canvas) {
            createjs.Stage.call(this, canvas);
        }).prototype = p = new createjs.Stage();

        p.setAutoPlay = function(autoPlay) {
            this.tickEnabled = autoPlay;
        }
        p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
        p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
        p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
        p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

        p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

        an.bootcompsLoaded = an.bootcompsLoaded || [];
        if(!an.bootstrapListeners) {
            an.bootstrapListeners=[];
        }

        an.bootstrapCallback=function(fnCallback) {
            an.bootstrapListeners.push(fnCallback);
            if(an.bootcompsLoaded.length > 0) {
                for(var i=0; i<an.bootcompsLoaded.length; ++i) {
                    fnCallback(an.bootcompsLoaded[i]);
                }
            }
        };

        an.compositions = an.compositions || {};
        an.compositions['559B450ECF44B84B824008B0C5B4BC45'] = {
            getStage: function() { return exportRoot.getStage(); },
            getLibrary: function() { return lib; },
            getSpriteSheet: function() { return ss; },
            getImages: function() { return img; }
        };

        an.compositionLoaded = function(id) {
            an.bootcompsLoaded.push(id);
            for(var j=0; j<an.bootstrapListeners.length; j++) {
                an.bootstrapListeners[j](id);
            }
        }

        an.getComposition = function(id) {
            return an.compositions[id];
        }


        an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {      
            var lastW, lastH, lastS=1;      
            window.addEventListener('resize', resizeCanvas);        
            resizeCanvas();     
            function resizeCanvas() {           
                var w = lib.properties.width, h = lib.properties.height;            
                var iw = window.innerWidth, ih=window.innerHeight;          
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;          
                if(isResp) {                
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
                        sRatio = lastS;                
                    }               
                    else if(!isScale) {                 
                        if(iw<w || ih<h)                        
                            sRatio = Math.min(xRatio, yRatio);              
                    }               
                    else if(scaleType==1) {                 
                        sRatio = Math.min(xRatio, yRatio);              
                    }               
                    else if(scaleType==2) {                 
                        sRatio = Math.max(xRatio, yRatio);              
                    }           
                }           
                domContainers[0].width = w * pRatio * sRatio;           
                domContainers[0].height = h * pRatio * sRatio;          
                domContainers.forEach(function(container) {             
                    container.style.width = w * sRatio + 'px';              
                    container.style.height = h * sRatio + 'px';         
                });         
                stage.scaleX = pRatio*sRatio;           
                stage.scaleY = pRatio*sRatio;           
                lastW = iw; lastH = ih; lastS = sRatio;            
                stage.tickOnUpdate = false;            
                stage.update();            
                stage.tickOnUpdate = true;      
            }
        }


        })(createjs = createjs||{}, AdobeAn = AdobeAn||{});
        var createjs, AdobeAn;

        var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
        function init() {
            canvas = document.getElementById("canvas");
            anim_container = document.getElementById("animation_container");
            dom_overlay_container = document.getElementById("dom_overlay_container");
            var comp=AdobeAn.getComposition("559B450ECF44B84B824008B0C5B4BC45");
            var lib=comp.getLibrary();
            handleComplete({},comp);
        }
        function handleComplete(evt,comp) {
            //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
            var lib=comp.getLibrary();
            var ss=comp.getSpriteSheet();
            exportRoot = new lib.CMA_Cross_S02();
            stage = new lib.Stage(canvas);  
            //Registers the "tick" event listener.
            fnStartAnimation = function() {
                stage.addChild(exportRoot);
                createjs.Ticker.setFPS(lib.properties.fps);
                createjs.Ticker.addEventListener("tick", stage);
            }       
            //Code to support hidpi screens and responsive scaling.
            AdobeAn.makeResponsive(false,'both',false,1,[canvas,anim_container,dom_overlay_container]); 
            AdobeAn.compositionLoaded(lib.properties.id);
            fnStartAnimation();
        }

    }
}