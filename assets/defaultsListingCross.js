{
  "title": "#gen:{listing_update:address}",
  "thumb": "https://s3.ap-southeast-2.amazonaws.com/com.homeprezzo.media/thumbs/Cross-Lisiting-Cover.jpg",
  "language": "en",
  "voiceover": "",
  "playback": "once",
  "privacy": "public",
  "listing_update_id": "#gen:{listing_update:listing_update_id}",
  "listing_id": "#gen:{listing_update:listing_id}",
  "sceneData": {
    "configuration": {
      "colorScheme": "#gen:{user_attr:defaultPalette:0,4,2}",
      "font": "InterstateCond",
      "listing": "#gen:{listing_update:id|listing_id,listing|listing_data}"
    },
    "scenes": [
      {
        "scene": "Listing_Cross_S01",
        "configuration": {
          "lcPhoto1":"#gen:{listing_update:images:1}"
        }
      },
      {
        "scene": "Listing_Cross_S02",
        "configuration": {
          "lcphoto2":"#gen:{listing_update:images:2}"
		}
      },
      {
        "scene": "Listing_Cross_S03",
        "configuration": {
          "lcphoto3":"#gen:{listing_update:images:3}"
		}
      },
      {
        "scene": "Listing_Cross_S04",
        "configuration": {
          "lcphoto4":"#gen:{listing_update:images:4}"
		}
      },
      {
        "scene": "Listing_Cross_S05",
        "configuration": {
          "lcphoto6":"#gen:{listing_update:images:5}",
		  "lcphoto7":"#gen:{listing_update:images:6}"
		}
      },
      {
        "scene": "Listing_Cross_S06",
        "configuration": {
          "description": {
            "description": "#gen:{listing_update:listing_data:residential[0]->description}|{sentence:0}"
          }
        }
      },
      {
        "scene": "Listing_Cross_S07",
        "configuration": {
          "lcphotog1":"#gen:{listing_update:images:7}",
          "inspectionTimes": {
            "data": [
              {
                "inspection": {
                  "inspection": "#gen:{listing_update:listing_data:residential[0]->inspectionTimes[0]->inspection}"
                }
              }
            ]
          }
        }
      },
      {
        "scene": "Listing_Cross_S08",
        "configuration": {
          "lcphotog2":"#gen:{listing_update:images:1}"
        }
      }
    ]
  }
}