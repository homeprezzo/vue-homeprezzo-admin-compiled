import axios from 'axios'
import store from 'src/store/index'

export const axe = axios
let prod = 'dev' // 'local' | 'dev' | 'live'
var _apiroute, _signed, _pub, _localapiroute, _loginchecker, _storage // eslint-disable-line no-unused-vars

if (prod === 'local') {
  _apiroute       = 'http://localhost:8000/api/'
  _signed         = 'http://localhost:8000/api/signed/'
  _localapiroute  = 'http://localhost:8000/api/'
  _pub            = 'http://localhost:8080/'
  _loginchecker   =  'http://localhost:8000/api/'
  _storage        = 'http://localhost:8000/storage/'
}

if (prod === 'dev') {
  _apiroute       = 'http://13.210.27.84/api/api/'
  _signed         = 'http://13.210.27.84/api/api/'
  _localapiroute  = 'http://13.210.27.84/api/'
  _pub            = 'http://localhost:8080/'
  _loginchecker   = 'http://localhost:8000/api/'
  _storage        = 'http://13.210.27.84/api/storage/'
}

if (prod === 'live') {
  _apiroute       = 'http://localhost:8000/api/'
  _signed         = 'http://localhost:8000/api/signed/'
  _localapiroute  = 'http://localhost:8000/api/'
  _pub            = 'http://localhost:8080/'
  _loginchecker   =  'http://localhost:8000/api/'
  _storage        = 'http://localhost:8000/storage/'
}

// assign api compiler values
export const _prod          = prod
export const apiroute       = _apiroute
export const storage        = _storage
export const signed         = _signed
export const localapiroute  = _localapiroute
export const pub            = _pub
export const loginchecker   = _loginchecker

export const route = {
  signin: apiroute + 'attempt',
  colours: apiroute + 'colors',
  raw:{
    build: 'https://stage.homeprezzo.com.au/api/test/autogens/build',
    load: 'https://stage.homeprezzo.com.au/api/test/prezzo/load',
    delete: 'https://stage.homeprezzo.com.au/api/test/prezzo/delete',
    saveProperty: 'https://stage.homeprezzo.com.au/api/test/option/save.json'
  },
  uploads: {
    image: apiroute + 'uploads/image',
    raw: apiroute + 'uploads/raw/read',
    thumb: {
      scene: scene => apiroute + 'thumb/scene/' + scene
    }
  },
  config: {
    types: apiroute + 'config/types'
  },
  jsscenes: {
    store: apiroute + 'js/store',
    check: apiroute + 'js/check',
    update: js => apiroute + 'js/' + js + '/update',
  },
  scenes: {
    get: apiroute + 'scenes',
    save: apiroute + 'scenes/save',
    store: apiroute + 'scenes/store',
    scene: scene => apiroute + 'scenes/' + scene,
    opts: scene => apiroute + 'scenes/' + scene + '/opts',
    approve: scene => apiroute + 'scenes/' + scene + '/approve',
    update: scene => apiroute + 'scenes/' + scene + '/update',
    deactivate: scene => apiroute + 'scenes/' + scene + '/deactivate',
    option: {
      add: scene => apiroute + 'scenes/' + scene + '/options/add',
      update: (scene, opt) => apiroute + 'scenes/' + scene + '/options/update/' + opt,
    },
    js: {
      update: (scene, js) => apiroute + 'scenes/' + scene + '/js/' + js + '/update',
    },
  },
  groups : {
    get: apiroute + 'groups',
    store: apiroute + 'groups/store',
    group: group => apiroute + 'groups/' + group,
    delete: group => apiroute + 'groups/' + group + '/remove',
    opts: {
      get: group => apiroute + 'groups/' + group + '/opts',
      store: group => apiroute + 'groups/' + group + '/opts',
      remove: (group, opt) => apiroute + 'groups/' + group + '/opts/' + opt + '/remove',
    },
    autogens: {
      store: group => apiroute + 'groups/' + group + '/autogen/store',
      show: group => apiroute + 'groups/' + group + '/autogen/show',
      update: (group, autogen) => `${apiroute}groups/${group}/autogen/${autogen}/update`,
    }
  },

}